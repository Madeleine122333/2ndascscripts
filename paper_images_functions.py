#!/usr/bin/env python

# import all relevant modules
import numpy as np
print("imported numpy")
import argparse
print("imported argparse")
import os
print("imported os")
import pdb
try:
	from ipdb import set_trace as stop
except:
	from pdb import set_trace as stop
print("imported pdb")
import matplotlib.pyplot as plt
import matplotlibrc
from matplotlib.colors import ListedColormap
from matplotlib import cm
from matplotlib.colors import LogNorm
from matplotlib.lines import Line2D
import matplotlib.gridspec as gridspec
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as ticker
from matplotlib.legend_handler import HandlerLine2D, HandlerTuple
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from matplotlib.container import ErrorbarContainer
from matplotlib.collections import LineCollection
from scipy.optimize import curve_fit
import random

# Extract the number from a line 
def extract_number(line, start_string=":", end_string="km/s", error=None):
		# find the indexes of the start and end strings 
        i_start = line.find(start_string) + len(start_string) + 1
        if end_string is None:
        	i_end = len(line)
        else:
        	i_end = line.rfind(end_string)

        # define number depending if the line includes an error for the number
        if error is None:
        	number = float(line[i_start:i_end])
        else:
        	i_error = line.find(error)
        	value = float(line[i_start:i_error])
        	error = float(line[i_error+len(error):i_end])
        	number = [value, error]

        return number

def gauss(x, mean, sigma):
	coeff = 1/(sigma * (2 * np.pi)**0.5)
	exp_part = np.exp(-(x-mean)**2/(2*sigma**2))
	return coeff * exp_part

def partial_gauss(sigma):
	def F(x, mean):
		return gauss(x, mean, sigma)
	return F

def linearFunc(x, grad, y_int):
	return x*grad+y_int

def linearFunc_int0(x, grad):
    return x*grad

def quadraticFunc(x, a, b, c):
	return a * x**2 + b * x + c

def get_fig_res_data(images_path, simulation_id, cutoff_str, fwhm):
	# define the file name to read
	filename = "{}/time_averages_data_table_{}_cutoff{}_r{}.txt".format(images_path, simulation_id, cutoff_str, fwhm)

	if os.path.exists(filename):
		f = open(filename, 'r')
		contents = f.readlines()

		# find 3D correction factor data
		indices = [i for i, val in enumerate(contents) if "+-" in val and "km/s" not in val and "disp" not in val]
		values = [extract_number(contents[i], end_string="+-") for i in indices]
		errors = [extract_number(contents[i], start_string="+-") for i in indices]

	else:
		print("file required for Figure res including fwhm {} is unavailable".format(fwhm))
		print("try running 'python paper_images.py --table --sim_id={} --fwhm={}'".format(simulation_id, fwhm))
		pdb.set_trace()
		exit()

	# find sigma for given decrease resolution, assuming the domain is 2.2 pc with 256 pixels (as a fraction of the cloud radius)
	fwhm_pc = fwhm * 2.2 / 256

	return [fwhm_pc, values, errors]

def time_average(t, s):
	# calculate the differences in successive times and values of the statistic
	t_differences = t[1:] - t[:-1]
	s_sums = s[1:] + s[:-1]

	return 0.5 * (t_differences * s_sums).sum() / (t[-1] - t[0])

def get_second_moment_data(data_path, cutoff_str, fwhm):
	#define the file name to read
	filename = "{}/Turbulence_Data_0000_r{}_{}.txt".format(data_path, fwhm, cutoff_str)

	if os.path.exists(filename):
		f = open(filename, 'r')
		contents = f.readlines()

		# find second moments
		indices = [i for i, val in enumerate(contents) if "sigma_i**2" in val]
		values = [np.sqrt(extract_number(contents[i], start_string = "=", end_string = "(km/s)**2")) for i in indices]

	else:
		print("file required for extracting second moment data ({}) is unavailable".format(filename))
		pdb.set_trace()
		exit()

	#find sigma for given decrease resolution, assuming the domain is 2.2 pc with 256 pixels
	fwhm_pc = fwhm * 2.2 / 256

	return [fwhm_pc, values]

def concatenate_rows(data_array, data_type, rows):
	all_data = []
	if data_type == "fwhm":
		for row in rows:
			row_data = np.array([data_array[i][row] for i in range(len(data_array))])
			all_data.append(row_data)
	elif data_type in ["value", "error"]:
		if data_type == "value":
			data_type_ind = 1
		else:
			data_type_ind = 2
		for row in rows:
			row_data = np.array([data_array[i][data_type_ind][row] for i in range(len(data_array))])
			all_data.append(row_data)
	else:
		print("data type must be 'fwhm', 'value' of 'error'")
		pdb.set_trace()
		exit()
	return np.concatenate(tuple(all_data))


def figure1(axis, times, zero_moments, first_moments, second_moments, min_maxs, save_info):
	fig, axes = plt.subplots(3, 3, figsize = (18,16.2))

	# plot zero moments
	vmin = 1e-21; vmax = 1e-18;
	cmap = plt.get_cmap('plasma')
	zero1 = axes[0,0].imshow(np.rot90(zero_moments["0"]), aspect = 1, extent = min_maxs[0],
		norm = LogNorm(vmin = vmin, vmax = vmax), cmap = cmap)
	zero2 = axes[0,1].imshow(np.rot90(zero_moments["1"]), aspect = 1, extent = min_maxs[1],
		norm = LogNorm(vmin = vmin, vmax = vmax), cmap = cmap)
	zero3 = axes[0,2].imshow(np.rot90(zero_moments["2"]), aspect = 1, extent = min_maxs[2],
		norm = LogNorm(vmin = vmin, vmax = vmax), cmap = cmap)

	# plot first moments
	vmin = -2; vmax = 2;
	cmap = plt.get_cmap('RdBu').reversed()
	first1 = axes[1,0].imshow(np.rot90(first_moments["0"]), aspect = 1, extent = min_maxs[0],
		vmin = vmin, vmax = vmax, cmap = cmap)
	first2 = axes[1,1].imshow(np.rot90(first_moments["1"]), aspect = 1, extent = min_maxs[1],
		vmin = vmin, vmax = vmax, cmap = cmap)
	first3 = axes[1,2].imshow(np.rot90(first_moments["2"]), aspect = 1, extent = min_maxs[2],
		vmin = vmin, vmax = vmax, cmap = cmap)

	# plot second moments
	vmin = 0; vmax = 1.4;
	cmap = plt.get_cmap('viridis')
	second1 = axes[2,0].imshow(np.rot90(second_moments["0"]), aspect = 1, extent = min_maxs[0],
		vmin = vmin, vmax = vmax, cmap = cmap)
	second2 = axes[2,1].imshow(np.rot90(second_moments["1"]), aspect = 1, extent = min_maxs[1],
		vmin = vmin, vmax = vmax, cmap = cmap)
	second3 = axes[2,2].imshow(np.rot90(second_moments["2"]), aspect = 1, extent = min_maxs[2],
		vmin = vmin, vmax = vmax, cmap = cmap)

	images = [zero1, zero2, zero3, first1, first2, first3, second1, second2, second3]

	fontsize = 20
	# make colour bars and set axes labels
	for ax in [axes[2,0], axes[2,1], axes[2,2]]:
		if axis == 'x':
			ax.set_xlabel(r'$\mathrm{y \; (pc)}$', fontsize = fontsize)
			first_cb_label = r'$v_x \; \mathrm{(km/s)}$'
			second_cb_label = r'$\sigma_{v_x} \; \mathrm{(km/s)}$'
		if axis == 'y':
			ax.set_xlabel(r'$\mathrm{x \; (pc)}$', fontsize = fontsize)
			first_cb_label = r'$v_y \; \mathrm{(km/s)}$'
			second_cb_label = r'$\sigma_{v_y} \; \mathrm{(km/s)}$'
		if axis == 'z':
			ax.set_xlabel(r'$\mathrm{x \; (pc)}$', fontsize = fontsize)
			first_cb_label = r'$v_z \; \mathrm{(km/s)}$'
			second_cb_label = r'$\sigma_{v_z} \; \mathrm{(km/s)}$'

	# set axes labels
	for ax in [axes[0,0], axes[1,0], axes[2,0]]:
		if axis == 'x':
			ax.set_ylabel(r'$\mathrm{z \; (pc)}$', fontsize = fontsize)
		if axis == 'y':
			ax.set_ylabel(r'$\mathrm{z \; (pc)}$', fontsize = fontsize)
		if axis == 'z':
			ax.set_ylabel(r'$\mathrm{y \; (pc)}$', fontsize = fontsize)
	# set axes tick label font sizes
	for ax in axes.flatten():
		ax.xaxis.set_major_locator(MultipleLocator(0.5))
		ax.tick_params(axis = 'both', which = 'major', labelsize = fontsize)
		ax.tick_params(axis = 'both', which = 'minor', labelsize = fontsize)

	# set axes time titles
	posx = 0.97; posy = 0.97
	bbox = dict(facecolor='white', edgecolor='white', boxstyle='round', pad = 0.01)
	for ax in [axes[0,0], axes[1,0], axes[2,0]]:
		ax.text(posx, posy, r'$\mathrm{t = 0}$', horizontalalignment = 'right', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	for ax in [axes[0,1], axes[1,1], axes[2,1]]:
		ax.text(posx, posy, r'$\mathrm{t = 0.5 \, t_{ff}}$', horizontalalignment = 'right', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	for ax in [axes[0,2], axes[1,2], axes[2,2]]:
		ax.text(posx, posy, r'$\mathrm{t = t_{ff}}$', horizontalalignment = 'right', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	# set aces moment titles
	posx = 0.03; posy = 0.97
	bbox = dict(facecolor='white', edgecolor='white', boxstyle='round', pad = 0.01)
	for ax in [axes[0,0], axes[0,1], axes[0,2]]:
		ax.text(posx, posy, r'$\mathrm{0}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	for ax in [axes[1,0], axes[1,1], axes[1,2]]:
		ax.text(posx, posy, r'$\mathrm{1st}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	for ax in [axes[2,0], axes[2,1], axes[2,2]]:
		ax.text(posx, posy, r'$\mathrm{2nd}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, bbox = bbox, fontsize = fontsize)

	cb1 = fig.colorbar(zero3, ax = axes[0,2], fraction=0.046, pad=0.04)
	cb2 = fig.colorbar(first3, ax = axes[1,2], fraction=0.046, pad=0.04)
	cb3 = fig.colorbar(second3, ax = axes[2,2], fraction=0.046, pad=0.04)

	cb1.set_label(r'$\mathrm{Density\; (g/cm^3)}$', rotation = 90, position = (1,0.5), fontsize = fontsize)
	cb2.set_label(first_cb_label, rotation = 90, position = (1,0.5), fontsize = fontsize)
	cb3.set_label(second_cb_label, rotation = 90, position = (1,0.5), labelpad = 10, fontsize = fontsize)

	cb1.ax.tick_params(labelsize = fontsize)
	cb2.ax.tick_params(labelsize = fontsize)
	cb3.ax.tick_params(labelsize = fontsize)

	cb1.ax.minorticks_off()

	plt.subplots_adjust(wspace=0.1, hspace=0.1)


	plt.savefig("{}/figure1_{}_cutoff{}_res{}axis{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3], axis)) #images path, sim id, cutoff, decrease res
	plt.clf()
	plt.close()
	print("saved {}/figure1_{}_cutoff{}_res{}axis{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3], axis))

def figure2(axis, first_moment_data, min_max, save_info, linear = False):
	fig = plt.figure(figsize=(18,10.8))
	spec = gridspec.GridSpec(ncols=6, nrows=2, figure=fig)
	ax1 = fig.add_subplot(spec[0, 0:2])
	ax2 = fig.add_subplot(spec[0, 2:4])
	ax3 = fig.add_subplot(spec[0, 4:])
	ax4 = fig.add_subplot(spec[1, 0:3])
	ax5 = fig.add_subplot(spec[1, 3:])

	vmin = -2; vmax = 2;
	cmap = plt.get_cmap('RdBu').reversed()
	first_moment = ax1.imshow(np.rot90(first_moment_data[0]), aspect = 1, extent = min_max, vmin = vmin, vmax = vmax, cmap = cmap)
	plane_fit = ax2.imshow(np.rot90(first_moment_data[1]), aspect = 1, extent = min_max, vmin = vmin, vmax = vmax, cmap = cmap)
	gradient_subtracted = ax3.imshow(np.rot90(first_moment_data[2]), aspect = 1, extent = min_max, vmin = vmin, vmax = vmax, cmap = cmap)

	# set font size
	font_size = 22

	# Make colour bars and set labels
	for ax in [ax1, ax2, ax3]:
		if axis == 'x':
			ax.set_xlabel(r'$\mathrm{y \; (pc)}$', fontsize = font_size)
			cb_label = r'$v_x \; \mathrm{(km/s)}$'
		if axis == 'y':
			ax.set_xlabel(r'$\mathrm{x \; (pc)}$', fontsize = font_size)
			cb_label = r'$v_y \; \mathrm{(km/s)}$'
			
		if axis == 'z':
			ax.set_xlabel(r'$\mathrm{x \; (pc)}$', fontsize = font_size)
			cb_label = r'$v_z \; \mathrm{(km/s)}$'
	# set axes tick label font sizes
	for ax in [ax1, ax2, ax3, ax4, ax5]:
		ax.xaxis.set_major_locator(MultipleLocator(0.5))
		ax.tick_params(axis = 'both', which = 'major', labelsize = font_size)
		ax.tick_params(axis = 'both', which = 'minor', labelsize = font_size)

	if axis == 'x':
		ax1.set_ylabel(r'$\mathrm{z \; (pc)}$', fontsize = font_size)
	if axis == 'y':
		ax1.set_ylabel(r'$\mathrm{z \; (pc)}$', fontsize = font_size)
	if axis == 'z':
		ax1.set_ylabel(r'$\mathrm{y \; (pc)}$', fontsize = font_size)

	cb = fig.colorbar(gradient_subtracted, ax = ax3, fraction=0.046, pad=0.04)
	cb.set_label(cb_label, rotation = 90, position = (1,0.5), fontsize = font_size)
	cb.ax.tick_params(labelsize = font_size)

	# make the pdf
	pdf_range = (min(vmin, np.nanmin(first_moment_data[0:2])), max(vmax, np.nanmax(first_moment_data[0:2])))
	pdf_before = np.histogram(first_moment_data[0][~np.isnan(first_moment_data[0])], bins = 200, range = pdf_range)
	pdf_after = np.histogram(first_moment_data[2][~np.isnan(first_moment_data[2])], bins = 200, range = pdf_range)
	
	# find the number of data points
	num_before = first_moment_data[0][~np.isnan(first_moment_data[0])].size
	num_after = first_moment_data[1][~np.isnan(first_moment_data[1])].size

	# define the centre positions and widths of bins
	positions_before = [(a + b)/2 for a, b in zip(pdf_before[1][1:], pdf_before[1][:-1])]
	bin_widths_before = [abs(a - b) for a, b in zip(pdf_before[1][1:], pdf_before[1][:-1])]
	positions_after = [(a + b)/2 for a, b in zip(pdf_after[1][1:], pdf_after[1][:-1])]
	bin_widths_after = [abs(a - b) for a, b in zip(pdf_after[1][1:], pdf_after[1][:-1])]

	# define the error bars 
	errors_before = [a**0.5 / (num_before * b) if a!=0 else 0 for a, b in zip(pdf_before[0], bin_widths_before)]
	errors_after = [a**0.5 / (num_after * b) if a!=0 else 0 for a, b in zip(pdf_after[0], bin_widths_after)]

	# define the frequencies
	frequencies_before = [a / (num_before * b) if a!=0 else 0 for a, b in zip(pdf_before[0], bin_widths_before)]
	frequencies_after = [a / (num_after * b) if a!=0 else 0 for a, b in zip(pdf_after[0], bin_widths_after)]

	# fit Gaussian distribution
	# define fitting range
	fit_min_before = -1; fit_max_before = 1
	fit_min_after = -1.5; fit_max_after = 1.5
	indices_before = [i for i, val in enumerate(positions_before) if val > fit_min_before and val < fit_max_before]
	indices_after = [i for i, val in enumerate(positions_after) if val > fit_min_after and val < fit_max_after]
	fit_positions_before = [val for i, val in enumerate(positions_before) if i in indices_before]
	fit_positions_after = [val for i, val in enumerate(positions_after) if i in indices_after]
	fit_frequencies_before = [val for i, val in enumerate(frequencies_before) if i in indices_before]
	fit_frequencies_after = [val for i, val in enumerate(frequencies_after) if i in indices_after]
	popt_before, pcov_before = curve_fit(gauss, fit_positions_before, fit_frequencies_before)
	popt_after, pcov_after = curve_fit(gauss, fit_positions_after, fit_frequencies_after)
	print("=============================")
	print("before")
	print("=============================")
	print("mean: {}+-{}".format(popt_before[0], np.sqrt(pcov_before[0,0])))
	print("sigma: {}+-{}".format(popt_before[1], np.sqrt(pcov_before[1,1])))
	print("=============================")
	print("after")
	print("=============================")
	print("mean: {}+-{}".format(popt_after[0], np.sqrt(pcov_after[0,0])))
	print("sigma: {}+-{}".format(popt_after[1], np.sqrt(pcov_after[1,1])))

	# plot pdfs with fitted gaussian distributions
	pdf_label = r'$\mathrm{PDF \; data}$'
	fit_label = r'$\mathrm{Gaussian \; fit}$'
	y_before = [gauss(x, np.nanmean(first_moment_data[0]),np.nanstd(first_moment_data[0])) for x in positions_before]
	y_after = [gauss(x, np.nanmean(first_moment_data[2]),np.nanstd(first_moment_data[2])) for x in positions_after]
	ax4.errorbar(positions_before, frequencies_before, yerr = errors_before, fmt = 'k.', label = pdf_label )
	ax4.plot(positions_before, gauss(positions_before, *popt_before), 'b-', label = fit_label) # y_before
	ax5.errorbar(positions_after, frequencies_after, yerr = errors_after, fmt = 'k.', label = pdf_label)
	ax5.plot(positions_after, gauss(positions_after, *popt_after), 'b-', label = fit_label) # y_after

	ymax = max(max(frequencies_before), max(frequencies_after))
	for ax in [ax4, ax5]:
		ax.set_xlabel(cb_label, fontsize=font_size)
		ax.set_xlim(vmin, vmax)
		ax.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), fontsize=font_size) #Was loc = (0.73, 0.85)
		if not linear:
			ax.set_yscale('log', basey=10)
			ax.set_ylim(1e-2,ymax*3)
			ax.tick_params(axis='y', which='minor', left=False, right=False)
		else:
			ax.set_ylim(0,ymax*1.1)

	ax4.set_ylabel(r'$\mathrm{PDF}$', fontsize=font_size)

	# Make headings
	posx = 0.03; posy = 0.97
	bbox = dict(facecolor='white', edgecolor='white', boxstyle='round', pad = 0.01)
	ax1.text(posx, posy, r'$\mathrm{1st}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax1.transAxes, fontsize=font_size)
	ax2.text(posx, posy, r'$\mathrm{Fitted\; gradient}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax2.transAxes, fontsize=font_size)
	ax3.text(posx, posy, r'$\mathrm{Gradient}$-$\mathrm{corrected \; 1st}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax3.transAxes, fontsize=font_size)
	ax4.text(posx, posy, r'$\mathrm{1st}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax4.transAxes, fontsize=font_size)
	ax5.text(posx, posy, r'$\mathrm{Gradient}$-$\mathrm{corrected\; 1st}$-$\mathrm{moment}$', horizontalalignment = 'left', verticalalignment = 'top', transform = ax5.transAxes, fontsize=font_size)

	plt.subplots_adjust(wspace=0.2, hspace=0.15)
	fig.tight_layout()

	plt.savefig("{}/figure2_{}_cutoff{}_res{}_axis{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3], axis)) #images path, sim id, cutoff, decrease res
	plt.clf()
	plt.close()
	print("saved {}/figure2_{}_cutoff{}_res{}_axis{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3], axis))

def figure3(t, first_moments, corrected_first_moments, second_moments, disp_3d_corr, save_info):
	# calculate sigma_p data
	sigma_p = np.sqrt(first_moments**2 + second_moments**2)
	corrected_sigma_p = np.sqrt(corrected_first_moments**2 + second_moments**2)
	
	fig, axes = plt.subplots(2, 2, figsize = (16, 9.6)) # original (20,6)

	# Set labels
	first_moment_label = r'$\sigma_{\mathrm{c}} \; (x, y, z)$'
	corrected_first_moment_label = r'$\sigma_{\mathrm{(c}-\mathrm{grad)}} \; (x, y, z)$'
	second_moment_label = r'$\sigma_\mathrm{i} \; (x,y,z)$'
	sigma_p_label = r'$\sigma_{\mathrm{p}} \; (x,y,z)$'
	corrected_sigma_p_label = r'$\sigma_{\mathrm{(p}-\mathrm{grad)}} \; (x,y,z)$'
	disp_3d_corr_label = r'$\sigma _{(v_\mathrm{3D} - v_\mathrm{rot})}$'
	heading1 = r'$\mathrm{Turbulence\; quantifiers\; based \; on \; 1st \; moment}$'
	heading2 = r'$\mathrm{Turbulence\; quantifiers\; based \; on \; 2nd \; moment}$'
	heading3 = r'$\mathrm{Turbulence\; quantifiers\; based \; on \; 1st \; and \; 2nd \; moments}$'
	y_axis_label = r'$\mathrm{Turbulence\; quantifier\; (km/s)}$'
	x_axis_label = r'$\mathrm{Time\; (percentage\; of\; freefall\; time)}$'

	markersize = 3.2 # was 4
	# First moment panel
	fx = axes[0,0].plot(t, first_moments[0], "bx-", markersize = markersize)
	fy = axes[0,0].plot(t, first_moments[1], "gx-", markersize = markersize)
	fz = axes[0,0].plot(t, first_moments[2], "rx-", markersize = markersize)
	cfx = axes[0,0].plot(t, corrected_first_moments[0], "bd-")
	cfy = axes[0,0].plot(t, corrected_first_moments[1], "gd-")
	cfz = axes[0,0].plot(t, corrected_first_moments[2], "rd-")
	tot1 = axes[0,0].plot(t, disp_3d_corr, "y.-")

	# Second moment panel
	sx = axes[0,1].plot(t, second_moments[0], "bs-", markersize = markersize)
	sy = axes[0,1].plot(t, second_moments[1], "gs-", markersize = markersize)
	sz = axes[0,1].plot(t, second_moments[2], "rs-", markersize = markersize)
	tot2 = axes[0,1].plot(t, disp_3d_corr, "y.-")

	# sigma_p panel
	px = axes[1,0].plot(t, sigma_p[0], "bv-", markersize = markersize)
	py = axes[1,0].plot(t, sigma_p[1], "gv-", markersize = markersize)
	pz = axes[1,0].plot(t, sigma_p[2], "rv-", markersize = markersize)
	tot3 = axes[1,0].plot(t, disp_3d_corr, "y.-")

	# corrected sigma_p panel
	cpx = axes[1,1].plot(t, corrected_sigma_p[0], "b*-")#, markersize = markersize)
	cpy = axes[1,1].plot(t, corrected_sigma_p[1], "g*-")#, markersize = markersize)
	cpz = axes[1,1].plot(t, corrected_sigma_p[2], "r*-")#, markersize = markersize)
	tot4 = axes[1,1].plot(t, disp_3d_corr, "y.-")

	fontsize = 18
	# Make labels and set limits
	for ax in axes.flatten():
		ax.set_xlabel(x_axis_label, fontsize = fontsize)
		ax.set_ylabel(y_axis_label, fontsize = fontsize)
		ax.set_xlim(0, 100)
		ax.set_ylim(0, 2)

	axes[0,0].text(0.02, 0.97, heading1, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[0,0].transAxes, fontsize = fontsize)
	axes[0,1].text(0.02, 0.97, heading2, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[0,1].transAxes, fontsize = fontsize)
	axes[1,0].text(0.02, 0.97, heading3, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[1,0].transAxes, fontsize = fontsize)
	axes[1,1].text(0.02, 0.97, heading3, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[1,1].transAxes, fontsize = fontsize)

	fontsize = 20
	# make the legends
	disp_3d_corr_marker = Line2D([],[], color = "y", marker = ".")
	axes[0,0].legend([(fx[0], fy[0], fz[0]), (cfx[0], cfy[0], cfz[0]), disp_3d_corr_marker],
		[first_moment_label, corrected_first_moment_label, disp_3d_corr_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = fontsize, handleheight = 1.5, labelspacing = 0.05)
	axes[0,1].legend([(sx[0], sy[0], sz[0]), disp_3d_corr_marker],
		[second_moment_label, disp_3d_corr_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = fontsize, handleheight = 1.5, labelspacing = 0.05)
	axes[1,0].legend([(px[0], py[0], pz[0]), disp_3d_corr_marker],
		[sigma_p_label, disp_3d_corr_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = fontsize, handleheight = 1.5, labelspacing = 0.05)
	axes[1,1].legend([(cpx[0], cpy[0], cpz[0]), disp_3d_corr_marker],
		[corrected_sigma_p_label, disp_3d_corr_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = fontsize, handleheight = 1.5, labelspacing = 0.05)

	plt.savefig("{}/figure3_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3])) #images path, sim id, cutoff, decrease res
	plt.clf()
	plt.close()
	print("saved {}/figure3_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3]))

def figure4(t, first_moments, corrected_first_moments, second_moments, disp_3d_corr, save_info):
	# calculate sigma_p data
	sigma_p = np.sqrt(first_moments**2 + second_moments**2)
	corrected_sigma_p = np.sqrt(corrected_first_moments**2 + second_moments**2)

	# define the correction factors and compute the mean and standard deviation
	first_moment_correction_factors = disp_3d_corr / corrected_first_moments
	first_moment_correction_factors_average = np.mean(first_moment_correction_factors, axis = 0)
	first_moment_correction_factors_std = np.std(first_moment_correction_factors, axis = 0)
	second_moment_correction_factors = disp_3d_corr / second_moments
	second_moment_correction_factors_average = np.mean(second_moment_correction_factors, axis = 0)
	second_moment_correction_factors_std = np.std(second_moment_correction_factors, axis = 0)
	sigma_p_correction_factors = disp_3d_corr / sigma_p
	sigma_p_correction_factors_average = np.mean(sigma_p_correction_factors, axis = 0)
	sigma_p_corrfection_factors_std = np.std(sigma_p_correction_factors, axis = 0)
	corrected_sigma_p_correction_factors = disp_3d_corr / corrected_sigma_p
	corrected_sigma_p_correction_factors_average = np.mean(corrected_sigma_p_correction_factors, axis = 0)
	corrected_sigma_p_correction_factors_std = np.std(corrected_sigma_p_correction_factors, axis = 0)
	
	# make figure and subplots
	fig, axes = plt.subplots(2, 2, figsize = (16, 9.6))

	# set labels
	first_moment_label = r'$\sigma_{\mathrm{(\mathrm{c}} - \mathrm{grad)}} / \sigma _{(v_\mathrm{3D} - v_\mathrm{rot})} \; (x,y,z)$'
	second_moment_label = r'$\sigma_{\mathrm{i}} / \sigma _{(v_\mathrm{3D} - v_\mathrm{rot})} \; (x,y,z)$'
	sigma_p_label = r'$\sigma_{\mathrm{p}} / \sigma _{(v_\mathrm{3D} - v_\mathrm{rot})}\; (x,y,z)$'
	corrected_sigma_p_label = r'$\sigma_{\mathrm{(p}-\mathrm{grad)}} / \sigma _{(v_\mathrm{3D} - v_\mathrm{rot})} \; (x,y,z)$'
	average_label = r'$\mathrm{average\; over}\; x,\, y,\, z$'
	heading1 = r'$\mathrm{Correction\; factor \; based \; on \; 1st \; moment}$'
	heading2 = r'$\mathrm{Correction\; factor \; based \; on \; 2nd \; moment}$'
	heading3 = r'$\mathrm{Correction \; factor \; based \; on \; 1st \; and \; 2nd \; moments}$'
	y_axis_label = r'$\mathrm{Correction\; factor}$'
	x_axis_label = r'$\mathrm{Time\; (percentage\; of\; freefall\; time)}$'

	# set marker size
	markersize = 3.2
	markersize2 = 3.6
	markersize3 = 4.5

	# plot first moment panel data
	favg = axes[0,0].errorbar(t, first_moment_correction_factors_average, yerr = first_moment_correction_factors_std, fmt = "k.-", label = average_label)
	fx = axes[0,0].plot(t, first_moment_correction_factors[0], "bd-")
	fy = axes[0,0].plot(t, first_moment_correction_factors[1], "gd-")
	fz = axes[0,0].plot(t, first_moment_correction_factors[2], "rd-")

	# plot second moment panel data
	savg = axes[0,1].errorbar(t, second_moment_correction_factors_average, yerr = second_moment_correction_factors_std, fmt = "k.-", label = average_label)
	sx = axes[0,1].plot(t, second_moment_correction_factors[0], "bs-", markersize = markersize)
	sy = axes[0,1].plot(t, second_moment_correction_factors[1], "gs-", markersize = markersize)
	sz = axes[0,1].plot(t, second_moment_correction_factors[2], "rs-", markersize = markersize)

	# plot third panel
	pavg = axes[1,0].errorbar(t, sigma_p_correction_factors_average, yerr = sigma_p_corrfection_factors_std, fmt = "k.-", label = average_label)
	px = axes[1,0].plot(t, sigma_p_correction_factors[0], "bv-", markersize = markersize2)
	py = axes[1,0].plot(t, sigma_p_correction_factors[1], "gv-", markersize = markersize2)
	pz = axes[1,0].plot(t, sigma_p_correction_factors[2], "rv-", markersize = markersize2)

	# plot fourth panel
	cpavg = axes[1,1].errorbar(t, corrected_sigma_p_correction_factors_average, yerr = corrected_sigma_p_correction_factors_std, fmt = "k.-", label = average_label)
	cpx = axes[1,1].plot(t, corrected_sigma_p_correction_factors[0], "b*-", markersize = markersize3)
	cpy = axes[1,1].plot(t, corrected_sigma_p_correction_factors[1], "g*-", markersize = markersize3)
	cpz = axes[1,1].plot(t, corrected_sigma_p_correction_factors[2], "r*-", markersize = markersize3)

	# make labels and set limits
	fontsize = 20
	for ax in axes.flatten():
		ax.set_xlabel(x_axis_label, fontsize = fontsize)
		ax.set_ylabel(y_axis_label, fontsize = fontsize)
		ax.set_xlim(0, 100)

	# set y limits
	for ax in axes[0]:
		if save_info[2] == '1.0':
			ax.set_ylim(1, 5.5)
		else:
			ax.set_ylim(1, 5)
	for ax in axes[1]:
		ax.set_ylim(0,4)

	axes[0,0].text(0.02, 0.97, heading1, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[0,0].transAxes, fontsize = fontsize)
	axes[0,1].text(0.02, 0.97, heading2, horizontalalignment = 'left', verticalalignment = 'top', transform = axes[0,1].transAxes, fontsize = fontsize)
	for ax in axes[1]:
		ax.text(0.02, 0.97, heading3, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, fontsize = fontsize)
	
	# make legends
	if save_info[2] == "1.0":
		loc1='lower left'
		loc2='upper left'
		bbox_to_anchor1=(-0.02, -0.02)
		bbox_to_anchor2=(-0.02, 0.95)
	else:
		loc1='upper left'
		loc2='upper left'
		bbox_to_anchor1=(-0.02, 0.95)
		bbox_to_anchor2=(-0.02, 0.95)
	# make the average marker
	line = Line2D([],[], marker = '.', color = 'k')
	barline = LineCollection(np.empty((0,0,0)), color = 'k')
	capline = Line2D([],[],ls = 'none', alpha = 0.0)
	avg_marker = ErrorbarContainer((line, [capline], [barline]), has_xerr=False, has_yerr=True)
	# legends
	axes[0,0].legend([(fx[0], fy[0], fz[0]), avg_marker],
		[first_moment_label, average_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = loc1, bbox_to_anchor=bbox_to_anchor1,
		fontsize = fontsize)
	axes[0,1].legend([(sx[0], sy[0], sz[0]), avg_marker],
		[second_moment_label, average_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = loc2, bbox_to_anchor=bbox_to_anchor2,
		fontsize = fontsize)
	axes[1,0].legend([(px[0], py[0], pz[0]), avg_marker],
		[sigma_p_label, average_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = loc2, bbox_to_anchor=bbox_to_anchor2,
		fontsize = fontsize)
	axes[1,1].legend([(cpx[0], cpy[0], cpz[0]), avg_marker],
		[corrected_sigma_p_label, average_label],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = loc2, bbox_to_anchor=bbox_to_anchor2,
		fontsize = fontsize)

	plt.savefig("{}/figure4_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3])) #images path, sim id, cutoff, decrease res
	plt.clf()
	plt.close()

	print("saved {}/figure4_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3]))

def figure5(total_data, save_info, linear = False):
	# set labels
	x_axis_label = r'$\mathrm{Telescope \; beam \; size \; FWHM/R}$'
	y_axis_label = r'$\mathrm{Correction\; factor}$'
	best_fits = r'$\mathrm{best \; fits}$'
	best_fit = r'$\mathrm{best \; fit}$'
	heading = r'$\mathrm{Telescope \; beam \; resolution \; study}$'
	first_moment_label = r'$\sigma_{(\mathrm{c}-\mathrm{grad)}} \; \mathrm{(} x, \, y, \, z\mathrm{)}$'# r'$\mathrm{gradient}$-$\mathrm{corrected \; 1st \; moment \; (}x, \, y, \, z\mathrm{)}$'
	second_moment_label = r'$\sigma_{\mathrm{i}} \mathrm{\; (}x, \, y, \, z\mathrm{)}$'
	sigma_p_label = r'$\sigma_\mathrm{p} \mathrm{\; (}x, \, y, \, z\mathrm{)}$'
	sigma_p_corr_label = r'$\sigma_{(\mathrm{p}-\mathrm{grad})} \mathrm{\; (}x, \, y, \, z\mathrm{)}$'


	# make data arrays
	fwhms = np.array([total_data[i][0] for i in range(len(total_data))])
	x_sigma_ps = np.array([total_data[i][1][8] for i in range(len(total_data))])
	y_sigma_ps = np.array([total_data[i][1][9] for i in range(len(total_data))])
	z_sigma_ps = np.array([total_data[i][1][10] for i in range(len(total_data))])
	x_sigma_p_corrs = np.array([total_data[i][1][12] for i in range(len(total_data))])
	y_sigma_p_corrs = np.array([total_data[i][1][13] for i in range(len(total_data))])
	z_sigma_p_corrs = np.array([total_data[i][1][14] for i in range(len(total_data))])
	x_first_moments = np.array([total_data[i][1][0] for i in range(len(total_data))])
	y_first_moments = np.array([total_data[i][1][1] for i in range(len(total_data))])
	z_first_moments = np.array([total_data[i][1][2] for i in range(len(total_data))])
	first_moments = np.concatenate((x_first_moments, y_first_moments, z_first_moments))
	x_second_moments = np.array([total_data[i][1][3] for i in range(len(total_data))])
	y_second_moments = np.array([total_data[i][1][4] for i in range(len(total_data))])
	z_second_moments = np.array([total_data[i][1][5] for i in range(len(total_data))])
	second_moments = np.concatenate((x_second_moments, y_second_moments, z_second_moments))
	x_first_moments_err = np.array([total_data[i][2][0] for i in range(len(total_data))])
	y_first_moments_err = np.array([total_data[i][2][1] for i in range(len(total_data))])
	z_first_moments_err = np.array([total_data[i][2][2] for i in range(len(total_data))])
	first_moments_err = np.concatenate((x_first_moments_err, y_first_moments_err, z_first_moments_err))
	x_second_moments_err = np.array([total_data[i][2][3] for i in range(len(total_data))])
	y_second_moments_err = np.array([total_data[i][2][4] for i in range(len(total_data))])
	z_second_moments_err = np.array([total_data[i][2][5] for i in range(len(total_data))])
	second_moments_err = np.concatenate((x_second_moments_err, y_second_moments_err, z_second_moments_err))
	x_sigma_ps_err = np.array([total_data[i][2][8] for i in range(len(total_data))])
	y_sigma_ps_err = np.array([total_data[i][2][9] for i in range(len(total_data))])
	z_sigma_ps_err = np.array([total_data[i][2][10] for i in range(len(total_data))])
	x_sigma_p_corrs_err = np.array([total_data[i][2][12] for i in range(len(total_data))])
	y_sigma_p_corrs_err = np.array([total_data[i][2][13] for i in range(len(total_data))])
	z_sigma_p_corrs_err = np.array([total_data[i][2][14] for i in range(len(total_data))])
	x = np.linspace(0,2,500)

	# make linear fits
	first_fit, first_cov = curve_fit(quadraticFunc, fwhms, first_moments, sigma = first_moments_err)
	second_fit, second_cov = curve_fit(linearFunc, fwhms, second_moments, sigma = second_moments_err)
	x_first_fit, x_first_cov = curve_fit(quadraticFunc, fwhms, x_first_moments, sigma = x_first_moments_err)
	y_first_fit, y_first_cov = curve_fit(quadraticFunc, fwhms, y_first_moments, sigma = y_first_moments_err)
	z_first_fit, z_first_cov = curve_fit(quadraticFunc, fwhms, z_first_moments, sigma = z_first_moments_err)
	x_second_fit, x_second_cov = curve_fit(linearFunc, fwhms, x_second_moments, sigma = x_second_moments_err)
	y_second_fit, y_second_cov = curve_fit(linearFunc, fwhms, y_second_moments, sigma = y_second_moments_err)
	z_second_fit, z_second_cov = curve_fit(linearFunc, fwhms, z_second_moments, sigma = z_second_moments_err)
	# sigma_p fit
	all_fwhms = np.concatenate((fwhms, fwhms, fwhms))
	all_sigma_p_corrs = np.concatenate((x_sigma_p_corrs, y_sigma_p_corrs, z_sigma_p_corrs))
	all_sigma_p_err = np.concatenate((x_sigma_p_corrs_err, y_sigma_p_corrs_err, z_sigma_p_corrs_err))
	sigma_p_fit, sigma_p_cov = curve_fit(linearFunc, all_fwhms, all_sigma_p_corrs, sigma = all_sigma_p_err)

	fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 8.4))
	# plot data points and fits
	markersize = 4 # was 3.2
	markersize2 = 8
	x_shift = 1.05
	fx = ax1.errorbar(fwhms, x_first_moments, yerr=x_first_moments_err, fmt = "bd", markeredgewidth = 0.5, markeredgecolor = "k")
	fy = ax1.errorbar(fwhms, y_first_moments, yerr=y_first_moments_err, fmt = "gd", markeredgewidth = 0.5, markeredgecolor = "k")
	fz = ax1.errorbar(fwhms, z_first_moments, yerr=z_first_moments_err, fmt = "rd", markeredgewidth = 0.5, markeredgecolor = "k")
	sx = ax1.errorbar(fwhms, x_second_moments, yerr=x_second_moments_err, fmt = "bs", markeredgewidth = 0.5, markeredgecolor = "k") #, markersize = markersize
	sy = ax1.errorbar(fwhms, y_second_moments, yerr=y_second_moments_err, fmt = "gs", markeredgewidth = 0.5, markeredgecolor = "k") #, markersize = markersize
	sz = ax1.errorbar(fwhms, z_second_moments, yerr=z_second_moments_err, fmt = "rs", markeredgewidth = 0.5, markeredgecolor = "k") #, markersize = markersize
	qfx, = ax1.plot(x, quadraticFunc(x, *x_first_fit), "b-")
	qfy, = ax1.plot(x, quadraticFunc(x, *y_first_fit), "g-")
	qfz, = ax1.plot(x, quadraticFunc(x, *z_first_fit), "r-")
	lfx, = ax1.plot(x, linearFunc(x, *x_second_fit), "b-")
	lfy, = ax1.plot(x, linearFunc(x, *y_second_fit), "g-")
	lfz, = ax1.plot(x, linearFunc(x, *z_second_fit), "r-")
	px = ax2.errorbar(fwhms*x_shift, x_sigma_ps, yerr = x_sigma_ps_err, fmt = "bv", markeredgewidth = 0.5, markeredgecolor = "k") #markersize = markersize2,
	py = ax2.errorbar(fwhms*x_shift, y_sigma_ps, yerr = y_sigma_ps_err, fmt = "gv", markeredgewidth = 0.5, markeredgecolor = "k") #markersize = markersize2,
	pz = ax2.errorbar(fwhms*x_shift, z_sigma_ps, yerr = z_sigma_ps_err, fmt = "rv", markeredgewidth = 0.5, markeredgecolor = "k") #markersize = markersize2,
	pcx = ax2.errorbar(fwhms/x_shift, x_sigma_p_corrs, yerr = x_sigma_p_corrs_err, fmt = "b*", markeredgewidth = 0.5, markeredgecolor = "k", markersize = markersize2)
	pcy = ax2.errorbar(fwhms/x_shift, y_sigma_p_corrs, yerr = y_sigma_p_corrs_err, fmt = "g*", markeredgewidth = 0.5, markeredgecolor = "k", markersize = markersize2)
	pcz = ax2.errorbar(fwhms/x_shift, z_sigma_p_corrs, yerr = z_sigma_p_corrs_err, fmt = "r*", markeredgewidth = 0.5, markeredgecolor = "k", markersize = markersize2)
	qfsig = ax2.plot(x, linearFunc(x, *sigma_p_fit), "k-")

	# set axes limits and scales
	for ax in [ax1, ax2]:
		ax.set_xlim(0.008, 1)
		if not linear:
			ax.set_xscale('log')
			ax1.set_yscale('log')
		ax.set_xticks([0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64])
		ax.xaxis.set_major_formatter(ScalarFormatter())
		ax.tick_params(axis='x', which='minor', bottom=False, top=False)
		ax.tick_params(axis='y', which='minor', bottom=False, top=False)

		# make labels
		font_size = 16
		ax.set_xlabel(x_axis_label, fontsize = font_size)
		ax.set_ylabel(y_axis_label, fontsize = font_size)
		ax.text(0.02, 0.97, heading, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, fontsize = font_size)
	ax1.set_ylim(1,20)
	ax2.set_ylim(0.5,3.5)
	
	# create the legend
	ax1.legend([(fx, fy, fz), (sx, sy, sz), (qfx, qfy, qfz)],
		[first_moment_label, second_moment_label, best_fits],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = font_size)
	# create marker for best fit
	best_fit_marker = Line2D([],[], color = 'black')
	ax2.legend([(px, py, pz), (pcx, pcy, pcz), best_fit_marker],
		[sigma_p_label, sigma_p_corr_label, best_fit],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = font_size)

	plt.savefig("{}/figure5_{}_cutoff{}.pdf".format(save_info[0], save_info[1], save_info[2]))
	print("saved {}/figure5_{}_cutoff{}.pdf".format(save_info[0], save_info[1], save_info[2]))
	
	# save the fit parameters to a txt file
	filename = "{}/resolution_study_fit_data_{}_cutoff{}_r{}.txt".format(save_info[0], save_info[1], save_info[2], save_info[3])
	f = open(filename, 'w+')
	f.write(filename)
	f.write("\n")
	f.write("\n=======================================================")
	f.write("\ngradient-corrected first moment correction factor fit")
	f.write("\n=======================================================")
	f.write("\ncorr_factor = a fwhm^2 + b fwhm + c")
	f.write("\n    a = {} +- {}".format(first_fit[0], np.sqrt(first_cov[0,0])))
	f.write("\n    b = {} +- {}".format(first_fit[1], np.sqrt(first_cov[1,1])))
	f.write("\n    c = {} +- {}".format(first_fit[2], np.sqrt(first_cov[2,2])))
	f.write("\n")
	f.write("\n=======================================================")
	f.write("\nsecond moment correction factor fit")
	f.write("\n=======================================================")
	f.write("\ncorr_factor = a fwhm + b")
	f.write("\n    a = {} +- {}".format(second_fit[0], np.sqrt(second_cov[0,0])))
	f.write("\n    b = {} +- {}".format(second_fit[1], np.sqrt(second_cov[1,1])))
	f.write("\n")
	f.write("\n=======================================================")
	f.write("\nsigma_p corrected correction factor fit")
	f.write("\n=======================================================")
	f.write("\ncorr_factor = a fwhm + b")
	f.write("\n    a = {} +- {}".format(sigma_p_fit[0], np.sqrt(sigma_p_cov[0,0])))
	f.write("\n    b = {} +- {}".format(sigma_p_fit[1], np.sqrt(sigma_p_cov[1,1])))
	f.close()
	print("wrote fit data to {}".format(filename))

	# save the fit parameters to a txt file
	filename = "{}/resolution_study_fit_data_{}_cutoff{}_r{}_axes.txt".format(save_info[0], save_info[1], save_info[2], save_info[3])
	f = open(filename, 'w+')
	f.write(filename)
	f.write("\n")
	f.write("\n=======================================================")
	f.write("\ngradient-corrected first moment correction factor fits")
	f.write("\n=======================================================")
	f.write("\nx-axis")
	f.write("\n    corr_factor = {} fwhm^2 + {} fwhm + {}".format(x_first_fit[0], x_first_fit[1], x_first_fit[2]))
	f.write("\n    covariance stuff: {}".format(x_first_cov))
	f.write("\n")
	f.write("\ny-axis")
	f.write("\n    corr_factor = {} fwhm^2 + {} fwhm + {}".format(y_first_fit[0], y_first_fit[1], y_first_fit[2]))
	f.write("\n    covariance stuff: {}".format(y_first_cov))
	f.write("\n")
	f.write("\nz-axis")
	f.write("\n    corr_factor = {} fwhm^2 + {} fwhm + {}".format(z_first_fit[0], z_first_fit[1], z_first_fit[2]))
	f.write("\n    covariance stuff: {}".format(z_first_cov))
	f.write("\n")
	f.write("\n=======================================================")
	f.write("\nsecond moment correction factor fits")
	f.write("\n=======================================================")
	f.write("\nx-axis")
	f.write("\n    corr_factor = {} fwhm + {}".format(x_second_fit[0], x_second_fit[1]))
	f.write("\n    covariance stuff: {}".format(x_second_cov))
	f.write("\n")
	f.write("\ny-axis")
	f.write("\n    corr_factor = {} fwhm + {}".format(y_second_fit[0], y_second_fit[1]))
	f.write("\n    covariance stuff: {}".format(y_second_cov))
	f.write("\n")
	f.write("\nz-axis")
	f.write("\n    corr_factor = {} fwhm + {}".format(z_second_fit[0], z_second_fit[1]))
	f.write("\n    covariance stuff: {}".format(z_second_cov))
	f.write("\n")
	f.close()
	print("wrote fit data to {}".format(filename))

	# save the fit parameters to a text file in latex table format
	filename = "{}/resolution_study_fit_data_{}_cotuff{}_r{}_latex.txt".format(save_info[0], save_info[1], save_info[2], save_info[3])
	f = open(filename, 'w+')
	f.write("\n=======================================================")
	f.write("\ngradient-corrected first moment correction factor fits")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\siggradKDgen$ & $x$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][0],
		x_first_fit[0], np.sqrt(x_first_cov[0,0]),
		x_first_fit[1], np.sqrt(x_first_cov[1,1]),
		x_first_fit[2], np.sqrt(x_first_cov[2,2])))
	f.write("\n")
	f.write(r'$\siggradKDgen$ & $y$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][0],
		y_first_fit[0], np.sqrt(y_first_cov[0,0]),
		y_first_fit[1], np.sqrt(y_first_cov[1,1]),
		y_first_fit[2], np.sqrt(y_first_cov[2,2])))
	f.write("\n")
	f.write(r'$\siggradKDgen$ & $z$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][0],
		z_first_fit[0], np.sqrt(z_first_cov[0,0]),
		z_first_fit[1], np.sqrt(z_first_cov[1,1]),
		z_first_fit[2], np.sqrt(z_first_cov[2,2])))
	f.write("\n\n=======================================================")
	f.write("\nsecond moment correction factor fits")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\sigtwoKDgen$ & $x$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][0],
		x_second_fit[0], np.sqrt(x_second_cov[0,0]),
		x_second_fit[1], np.sqrt(x_second_cov[1,1])))
	f.write("\n")
	f.write(r'$\sigtwoKDgen$ & $y$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][0],
		y_second_fit[0], np.sqrt(y_second_cov[0,0]),
		y_second_fit[1], np.sqrt(y_second_cov[1,1])))
	f.write("\n")
	f.write(r'$\sigtwoKDgen$ & $z$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][0],
		z_second_fit[0], np.sqrt(z_second_cov[0,0]),
		z_second_fit[1], np.sqrt(z_second_cov[1,1])))
	f.write("\n\n=======================================================")
	f.write("\nsigma_p_corrected correction factor fit")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\sigpgradKDgen$ & all & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][0],
		sigma_p_fit[0], np.sqrt(sigma_p_cov[0,0]),
		sigma_p_fit[1], np.sqrt(sigma_p_cov[1,1])))
	f.close()
	print("wrote fit data in latex table format to {}".format(filename))

def figure6(total_data1, total_data2, total_data3, save_info):
	# set labels
	x_axis_label = r'$\mathrm{Telescope \; beam \; size \; FWHM/R}$'
	y_axis_label = r'$\mathrm{Correction\; factor}$'
	first_moments_label = r'$\sigma_{(\mathrm{c}-\mathrm{grad)}} \; \mathrm{(no, \;  mild \; and \; high \; rotation)}$' #r'$\mathrm{gradient}$-$\mathrm{corrected \; 1st \; moment \; (no, \;  mild \; and \; high \; rotation)}$'
	second_moments_label = r'$\sigma_{\mathrm{i}} \; \mathrm{(no, \;  mild \; and \; high \; rotation)}$' #r'$\mathrm{2nd \; moment \; (no, \;  mild \; and \; high \; rotation)}$'
	sigma_ps_label = r'$\sigma_\mathrm{p}\mathrm{\; (no, \;  mild \; and \; high \; rotation)}$'
	sigma_p_corrs_label = r'$\sigma_{\mathrm{(p}-\mathrm{grad)}}\mathrm{\; (no, \;  mild \; and \; high \; rotation)}$'
	best_fits = r'$\mathrm{best \; fits}$'
	best_fit = r'$\mathrm{best \; fit}$'
	heading = r'$\mathrm{Telescope \; beam \; resolution \; study}$'

	# get data
	# first simulation
	fwhms1_avg = np.array([total_data1[i][0] for i in range(len(total_data1))])
	first_moments1_avg = np.array([total_data1[i][1][6] for i in range(len(total_data1))])
	second_moments1_avg = np.array([total_data1[i][1][7] for i in range(len(total_data1))])
	sigma_ps1_avg = np.array([total_data1[i][1][11] for i in range(len(total_data1))])
	sigma_p_corrs1_avg = np.array([total_data1[i][1][15] for i in range(len(total_data1))])
	first_moments_err1_avg = np.array([total_data1[i][2][6] for i in range(len(total_data1))])
	second_moments_err1_avg = np.array([total_data1[i][2][7] for i in range(len(total_data1))])
	sigma_ps_err1_avg = np.array([total_data1[i][2][11] for i in range(len(total_data1))])
	sigma_p_corrs_err1_avg = np.array([total_data1[i][2][15] for i in range(len(total_data1))])
	fwhms1_all = concatenate_rows(total_data1, "fwhm", [0,0,0])
	first_moments1_all = concatenate_rows(total_data1, "value", [0,1,2])
	second_moments1_all = concatenate_rows(total_data1, "value", [3,4,5])
	sigma_ps1_all = concatenate_rows(total_data1, "value", [8,9,10])
	sigma_p_corrs1_all = concatenate_rows(total_data1, "value", [12,13,14])
	first_moments_err1_all = concatenate_rows(total_data1, "error", [0,1,2])
	second_moments_err1_all = concatenate_rows(total_data1, "error", [3,4,5])
	sigma_ps_err1_all = concatenate_rows(total_data1, "error", [8,9,10])
	sigma_p_corrs_err1_all = concatenate_rows(total_data1, "error", [12,13,14])

	# second simulation
	fwhms2_avg = np.array([total_data2[i][0] for i in range(len(total_data2))])
	first_moments2_avg = np.array([total_data2[i][1][6] for i in range(len(total_data2))])
	second_moments2_avg = np.array([total_data2[i][1][7] for i in range(len(total_data2))])
	sigma_ps2_avg = np.array([total_data2[i][1][11] for i in range(len(total_data2))])
	sigma_p_corrs2_avg = np.array([total_data2[i][1][15] for i in range(len(total_data2))])
	first_moments_err2_avg = np.array([total_data2[i][2][6] for i in range(len(total_data2))])
	second_moments_err2_avg = np.array([total_data2[i][2][7] for i in range(len(total_data2))])
	sigma_ps_err2_avg = np.array([total_data2[i][2][11] for i in range(len(total_data2))])
	sigma_p_corrs_err2_avg = np.array([total_data2[i][2][15] for i in range(len(total_data2))])
	fwhms2_all = concatenate_rows(total_data2, "fwhm", [0,0,0])
	first_moments2_all = concatenate_rows(total_data2, "value", [0,1,2])
	second_moments2_all = concatenate_rows(total_data2, "value", [3,4,5])
	sigma_ps2_all = concatenate_rows(total_data2, "value", [8,9,10])
	sigma_p_corrs2_all = concatenate_rows(total_data2, "value", [12,13,14])
	first_moments_err2_all = concatenate_rows(total_data2, "error", [0,1,2])
	second_moments_err2_all = concatenate_rows(total_data2, "error", [3,4,5])
	sigma_ps_err2_all = concatenate_rows(total_data2, "error", [8,9,10])
	sigma_p_corrs_err2_all = concatenate_rows(total_data2, "error", [12,13,14])

	# third simulation
	fwhms3_avg = np.array([total_data3[i][0] for i in range(len(total_data3))])
	first_moments3_avg = np.array([total_data3[i][1][6] for i in range(len(total_data3))])
	second_moments3_avg = np.array([total_data3[i][1][7] for i in range(len(total_data3))])
	sigma_ps3_avg = np.array([total_data3[i][1][11] for i in range(len(total_data3))])
	sigma_p_corrs3_avg = np.array([total_data3[i][1][15] for i in range(len(total_data3))])
	first_moments_err3_avg = np.array([total_data3[i][2][6] for i in range(len(total_data3))])
	second_moments_err3_avg = np.array([total_data3[i][2][7] for i in range(len(total_data3))])
	sigma_ps_err3_avg = np.array([total_data3[i][2][11] for i in range(len(total_data3))])
	sigma_p_corrs_err3_avg = np.array([total_data3[i][2][15] for i in range(len(total_data3))])
	fwhms3_all = concatenate_rows(total_data3, "fwhm", [0,0,0])
	first_moments3_all = concatenate_rows(total_data3, "value", [0,1,2])
	second_moments3_all = concatenate_rows(total_data3, "value", [3,4,5])
	sigma_ps3_all = concatenate_rows(total_data3, "value", [8,9,10])
	sigma_p_corrs3_all = concatenate_rows(total_data3, "value", [12,13,14])
	first_moments_err3_all = concatenate_rows(total_data3, "error", [0,1,2])
	second_moments_err3_all = concatenate_rows(total_data3, "error", [3,4,5])
	sigma_ps_err3_all = concatenate_rows(total_data3, "error", [8,9,10])
	sigma_p_corrs_err3_all = concatenate_rows(total_data3, "error", [12,13,14])

	# make linear fits
	# first simulation
	first_fit1, first_cov1 = curve_fit(quadraticFunc, fwhms1_all, first_moments1_all, sigma = first_moments_err1_all)
	second_fit1, second_cov1 = curve_fit(linearFunc, fwhms1_all, second_moments1_all, sigma = second_moments_err1_all)
	# second simulation
	first_fit2, first_cov2 = curve_fit(quadraticFunc, fwhms2_all, first_moments2_all, sigma = first_moments_err2_all)
	second_fit2, second_cov2 = curve_fit(linearFunc, fwhms2_all, second_moments2_all, sigma = second_moments_err2_all)
	# third simulation
	first_fit3, first_cov3 = curve_fit(quadraticFunc, fwhms3_all, first_moments3_all, sigma = first_moments_err3_all)
	second_fit3, second_cov3 = curve_fit(linearFunc, fwhms3_all, second_moments3_all, sigma = second_moments_err3_all)
	# sigma_p fit
	all_fwhms = np.concatenate((fwhms1_all, fwhms2_all, fwhms3_all))
	all_sigma_p_corrs = np.concatenate((sigma_p_corrs1_all, sigma_p_corrs2_all, sigma_p_corrs3_all))
	all_sigma_p_corrs_err = np.concatenate((sigma_p_corrs_err1_all, sigma_p_corrs_err2_all, sigma_p_corrs_err3_all))
	sigma_p_fit, sigma_p_cov = curve_fit(linearFunc, all_fwhms, all_sigma_p_corrs, sigma = all_sigma_p_corrs_err)
	
	x = np.linspace(0,2,500)

	fig, (ax1, ax2) = plt.subplots(2,1, figsize = (7,8.4))
	
	# plot data and best fits
	markersize = 8
	x_shift = 1.05
	f1 = ax1.errorbar(fwhms1_avg, first_moments1_avg, yerr = first_moments_err1_avg, fmt = "md", markeredgewidth = 0.5, markeredgecolor = "k")
	s1 = ax1.errorbar(fwhms1_avg, second_moments1_avg, yerr = second_moments_err1_avg, fmt = "ms", markeredgewidth = 0.5, markeredgecolor = "k")
	p1 = ax2.errorbar(fwhms1_avg*x_shift, sigma_ps1_avg, yerr = sigma_ps_err1_avg, fmt = "mv", markeredgewidth = 0.5, markeredgecolor = "k")
	pc1 = ax2.errorbar(fwhms1_avg/x_shift, sigma_p_corrs1_avg, yerr = sigma_p_corrs_err1_avg, fmt = "m*", markersize = markersize, markeredgewidth = 0.5, markeredgecolor = "k")
	f2 = ax1.errorbar(fwhms2_avg, first_moments2_avg, yerr = first_moments_err2_avg, fmt = "cd", markeredgewidth = 0.5, markeredgecolor = "k")
	s2 = ax1.errorbar(fwhms2_avg, second_moments2_avg, yerr = second_moments_err2_avg, fmt = "cs", markeredgewidth = 0.5, markeredgecolor = "k")
	p2 = ax2.errorbar(fwhms2_avg*x_shift, sigma_ps2_avg, yerr = sigma_ps_err2_avg, fmt = "cv", markeredgewidth = 0.5, markeredgecolor = "k")
	pc2 = ax2.errorbar(fwhms2_avg/x_shift, sigma_p_corrs2_avg, yerr = sigma_p_corrs_err2_avg, fmt = "c*", markersize = markersize, markeredgewidth = 0.5, markeredgecolor = "k")
	f3 = ax1.errorbar(fwhms3_avg, first_moments3_avg, yerr = first_moments_err3_avg, fmt = "yd", markeredgewidth = 0.5, markeredgecolor = "k")
	s3 = ax1.errorbar(fwhms3_avg, second_moments3_avg, yerr = second_moments_err3_avg, fmt = "ys", markeredgewidth = 0.5, markeredgecolor = "k")
	p3 = ax2.errorbar(fwhms3_avg*x_shift, sigma_ps3_avg, yerr = sigma_ps_err3_avg, fmt = "yv", markeredgewidth = 0.5, markeredgecolor = "k")
	pc3 = ax2.errorbar(fwhms3_avg/x_shift, sigma_p_corrs3_avg, yerr = sigma_p_corrs_err3_avg, fmt = "y*", markersize = markersize, markeredgewidth = 0.5, markeredgecolor = "k")
	qf1, = ax1.plot(x, quadraticFunc(x, *first_fit1), "m-")
	qf2, = ax1.plot(x, quadraticFunc(x, *first_fit2), "c-")
	qf3, = ax1.plot(x, quadraticFunc(x, *first_fit3), "y-")
	lf1, = ax1.plot(x, linearFunc(x, *second_fit1), "m-")
	lf2, = ax1.plot(x, linearFunc(x, *second_fit2), "c-")
	lf3, = ax1.plot(x, linearFunc(x, *second_fit3), "y-")
	qf4, = ax2.plot(x, linearFunc(x, *sigma_p_fit), "k-")

	for ax in [ax1, ax2]:
		ax.set_xscale('log')
		ax.set_xlim(0.008, 1)
		ax.set_xticks([0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64])
		ax.xaxis.set_major_formatter(ScalarFormatter())
		ax.tick_params(axis='x', which='minor', bottom=False, top=False)
		ax.tick_params(axis='y', which='minor', bottom=False, top=False)
	ax1.set_ylim(1,25) # was (1,25)
	ax2.set_ylim(0.5,3.5)
	ax1.set_yscale('log')
	
	# make labels
	font_size = 16
	for ax in [ax1, ax2]:
		ax.set_xlabel(x_axis_label, fontsize = font_size)
		ax.set_ylabel(y_axis_label, fontsize = font_size)
		ax.text(0.02, 0.97, heading, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, fontsize = font_size)

	# make legend
	ax1.legend([(f1, f2, f3), (s1, s2, s3), (qf1, qf2, qf3)],
		[first_moments_label, second_moments_label, best_fits],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = font_size)
	# create marker for best fit
	best_fit_marker = Line2D([],[], color = 'black')
	ax2.legend([(p1, p2, p3), (pc1, pc2, pc3),best_fit_marker],
		[sigma_ps_label, sigma_p_corrs_label, best_fit],
		handler_map={tuple: HandlerTuple(ndivide=None)}, loc = 'upper left', bbox_to_anchor=(0, 0.95),
		fontsize = font_size)

	# save figure
	plt.savefig("{}/figure6_{}_cutoff{}.pdf".format(save_info[0], save_info[1], save_info[2]))
	print("saved {}/figure6_{}_cutoff{}.pdf".format(save_info[0], save_info[1], save_info[2]))

	# save the fit parameters to a text file in latex table format
	filename = "{}/rotation_study_fit_data_{}_cotuff{}_r{}_latex.txt".format(save_info[0], save_info[1], save_info[2], save_info[3])
	f = open(filename, 'w+')
	f.write("\n=======================================================")
	f.write("\ngradient-corrected first moment correction factor fits")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\siggradKDgen$ & avg & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][0],
		first_fit1[0], np.sqrt(first_cov1[0,0]),
		first_fit1[1], np.sqrt(first_cov1[1,1]),
		first_fit1[2], np.sqrt(first_cov1[2,2])))
	f.write("\n")
	f.write(r'$\siggradKDgen$ & avg & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][1],
		first_fit2[0], np.sqrt(first_cov2[0,0]),
		first_fit2[1], np.sqrt(first_cov2[1,1]),
		first_fit2[2], np.sqrt(first_cov2[2,2])))
	f.write("\n")
	f.write(r'$\siggradKDgen$ & avg & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(save_info[4][2],
		first_fit3[0], np.sqrt(first_cov3[0,0]),
		first_fit3[1], np.sqrt(first_cov3[1,1]),
		first_fit3[2], np.sqrt(first_cov3[2,2])))
	f.write("\n\n=======================================================")
	f.write("\nsecond moment correction factor fits")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\sigtwoKDgen$ & avg & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][0],
		second_fit1[0], np.sqrt(second_cov1[0,0]),
		second_fit1[1], np.sqrt(second_cov1[1,1])))
	f.write("\n")
	f.write(r'$\sigtwoKDgen$ & avg & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][1],
		second_fit2[0], np.sqrt(second_cov2[0,0]),
		second_fit2[1], np.sqrt(second_cov2[1,1])))
	f.write("\n")
	f.write(r'$\sigtwoKDgen$ & avg & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(save_info[4][2],
		second_fit3[0], np.sqrt(second_cov3[0,0]),
		second_fit3[1], np.sqrt(second_cov3[1,1])))
	f.write("\n\n=======================================================")
	f.write("\nsigma_p_corrected correction factor fit")
	f.write("\ncorr_factor = p2 fwhm^2 + p1 fwhm + p0")
	f.write("\n=======================================================\n")
	f.write(r'$\sigpgradKDgen$ & avg & all & $0$ & ${0:.2f} \pm {1:.2f}$ & ${2:.2f} \pm {3:.2f}$\\'.format(sigma_p_fit[0], np.sqrt(sigma_p_cov[0,0]),
		sigma_p_fit[1], np.sqrt(sigma_p_cov[1,1])))
	f.close()
	print("wrote fit data in latex table format to {}".format(filename))

# where save_info = [images_path, simulation_id, cutoff_str, decrease_resolution_factor]
def table_data(times, first_moments, corrected_first_moments, second_moments, disp_3d_corr, save_info):
	# define the file name
	filename = "{}/time_averages_data_table_{}_cutoff{}_r{}.txt".format(save_info[0], save_info[1], save_info[2], save_info[3])

	# define the correction factors and compute the mean and standard deviation
	first_moment_correction_factors = disp_3d_corr / corrected_first_moments
	first_moment_correction_factors_average = np.mean(first_moment_correction_factors, axis = 0)
	second_moment_correction_factors = disp_3d_corr / second_moments
	second_moment_correction_factors_average = np.mean(second_moment_correction_factors, axis = 0)

	# define the Dickman and Kleiner statistics and compute the mean and stanard deviation
	sigma_p = np.sqrt(first_moments**2 + second_moments**2)
	sigma_p_correction_factors = disp_3d_corr / sigma_p
	sigma_p_corr = np.sqrt(corrected_first_moments**2 + second_moments**2)
	sigma_p_corr_correction_factors = disp_3d_corr / sigma_p_corr

	# delete the pre-existing file if it exists
	if os.path.exists(filename):
		os.remove(filename)
		print("deleted {}".format(filename))

	f = open(filename, 'w+')
	f.write(filename)

	f.write("\naverage values")
	f.write("\nfirst moment")
	f.write("\nx: {} +- {} km/s".format(time_average(times, first_moments[0]), np.std(first_moments[0])))
	f.write("\ny: {} +- {} km/s".format(time_average(times, first_moments[1]), np.std(first_moments[1])))
	f.write("\nz: {} +- {} km/s".format(time_average(times, first_moments[2]), np.std(first_moments[2])))
	f.write("\ngradient corrected first moment")
	f.write("\nx: {} +- {} km/s".format(time_average(times, corrected_first_moments[0]), np.std(corrected_first_moments[0])))
	f.write("\ny: {} +- {} km/s".format(time_average(times, corrected_first_moments[1]), np.std(corrected_first_moments[1])))
	f.write("\nz: {} +- {} km/s".format(time_average(times, corrected_first_moments[2]), np.std(corrected_first_moments[2])))
	f.write("\nsecond moment")
	f.write("\nx: {} +- {} km/s".format(time_average(times, second_moments[0]), np.std(second_moments[0])))
	f.write("\ny: {} +- {} km/s".format(time_average(times, second_moments[1]), np.std(second_moments[1])))
	f.write("\nz: {} +- {} km/s".format(time_average(times, second_moments[2]), np.std(second_moments[2])))
	f.write("\n3d")
	f.write("\nfirst: {} +- {} km/s".format(time_average(times, np.mean(first_moments, axis = 0)), np.std(first_moments)))
	f.write("\ngradient corrected first moment: {} +- {} km/s".format(time_average(times, np.mean(corrected_first_moments, axis = 0)), np.std(corrected_first_moments)))
	f.write("\nsecond: {} +- {} km/s\n".format(time_average(times, np.mean(second_moments, axis = 0)), np.std(second_moments)))

	f.write("\ncorrection factors")
	f.write("\nfirst moment")
	f.write("\nx: {} +- {}".format(time_average(times, first_moment_correction_factors[0]), np.std(first_moment_correction_factors[0])))
	f.write("\ny: {} +- {}".format(time_average(times, first_moment_correction_factors[1]), np.std(first_moment_correction_factors[1])))
	f.write("\nz: {} +- {}".format(time_average(times, first_moment_correction_factors[2]), np.std(first_moment_correction_factors[2])))
	f.write("\nsecond moment")
	f.write("\nx: {} +- {}".format(time_average(times, second_moment_correction_factors[0]), np.std(second_moment_correction_factors[0])))
	f.write("\ny: {} +- {}".format(time_average(times, second_moment_correction_factors[1]), np.std(second_moment_correction_factors[1])))
	f.write("\nz: {} +- {}".format(time_average(times, second_moment_correction_factors[2]), np.std(second_moment_correction_factors[2])))
	f.write("\n3d")
	f.write("\nfirst: {} +- {}".format(time_average(times, first_moment_correction_factors_average), np.std(first_moment_correction_factors)))
	f.write("\nsecond: {} +- {}".format(time_average(times, second_moment_correction_factors_average), np.std(second_moment_correction_factors)))

	f.write("\n\naverage disp_3d_corr: {} +- {}".format(time_average(times, disp_3d_corr), np.std(disp_3d_corr)))

	f.write("\n\nDickman and Kleiner stuff")
	f.write("\naverage values")
	f.write("\nsigma_p")
	f.write("\nx: {} +- {} km/s".format(time_average(times, sigma_p[0]), np.std(sigma_p[0])))
	f.write("\ny: {} +- {} km/s".format(time_average(times, sigma_p[1]), np.std(sigma_p[1])))
	f.write("\nz: {} +- {} km/s".format(time_average(times, sigma_p[2]), np.std(sigma_p[2])))
	f.write("\n3d: {} +- {} km/s".format(time_average(times, np.mean(sigma_p, axis = 0)), np.std(sigma_p)))
	f.write("\nsigma_p corrected")
	f.write("\nx: {} +- {} km/s".format(time_average(times, sigma_p_corr[0]), np.std(sigma_p_corr[0])))
	f.write("\ny: {} +- {} km/s".format(time_average(times, sigma_p_corr[1]), np.std(sigma_p_corr[1])))
	f.write("\nz: {} +- {} km/s".format(time_average(times, sigma_p_corr[2]), np.std(sigma_p_corr[2])))
	f.write("\n3d: {} +- {} km/s".format(time_average(times, np.mean(sigma_p_corr, axis = 0)), np.std(sigma_p_corr)))

	f.write("\n\ncorrection factors")
	f.write("\nsigma_p")
	f.write("\nx: {} +- {}".format(time_average(times, sigma_p_correction_factors[0]), np.std(sigma_p_correction_factors[0])))
	f.write("\ny: {} +- {}".format(time_average(times, sigma_p_correction_factors[1]), np.std(sigma_p_correction_factors[1])))
	f.write("\nz: {} +- {}".format(time_average(times, sigma_p_correction_factors[2]), np.std(sigma_p_correction_factors[2])))
	f.write("\n3d: {} +- {}".format(time_average(times, np.mean(sigma_p_correction_factors, axis = 0)), np.std(sigma_p_correction_factors)))
	f.write("\nsigma_p corrected")
	f.write("\nx: {} +- {}".format(time_average(times, sigma_p_corr_correction_factors[0]), np.std(sigma_p_corr_correction_factors[0])))
	f.write("\ny: {} +- {}".format(time_average(times, sigma_p_corr_correction_factors[1]), np.std(sigma_p_corr_correction_factors[1])))
	f.write("\nz: {} +- {}".format(time_average(times, sigma_p_corr_correction_factors[2]), np.std(sigma_p_corr_correction_factors[2])))
	f.write("\n3d: {} +- {}".format(time_average(times, np.mean(sigma_p_corr_correction_factors, axis = 0)), np.std(sigma_p_corr_correction_factors)))

	f.close()
	print("wrote output data to {}".format(filename))

def best_fits(total_data1, total_data2, total_data3, save_info):

	# generates a numpy array with n uniformly distributed random numbers based on min and max
	def generate_random_uniform_numbers(n=100, min=0.0, max=1.0, seed=None):
		random.seed(seed) # set the random seed; if None, random uses the system time
		random_numbers = [random.uniform(min, max) for _ in range(n)]
		return np.array(random_numbers)

	# special function to get more reasonable uncertinty estimates in the fits
	def special_curve_fit(func, xin, yin, n_samples=5000, quiet=False):
		# get unique x values and respective y values
		unique_x = np.unique(xin)
		unique_y = np.array([yin[xin==unique_x[i]] for i in range(0, len(unique_x))])
		inds = np.array([generate_random_uniform_numbers(n=n_samples, min=0, max=len(unique_y[i])-1, seed=i) for i in range(0, len(unique_x))]).astype(int)
		popts = []
		perrs = []
		for i in range(0, n_samples):
			y = [unique_y[j,inds[j,i]] for j in range(0, len(unique_x))]
			popt, perr = curve_fit(func, unique_x, y)
			popts.append(popt)
			perrs.append(perr)
		popts = np.array(popts)
		perrs = np.array(perrs)
		ret_popt = np.zeros(len(popt))
		ret_perr = np.zeros((len(popt),len(popt)))
		for i in range(0, len(popt)):
			bestval = np.mean(popts[:,i])
			variation = np.std(popts[:,i])
			error = np.mean(np.sqrt(perrs[:,i,i]))
			if i==len(popt)-1:
				uncert = error + variation # for p0, we add the variations; this reflects better the variation in each FWHM bin
			else:
				uncert = error
			ret_popt[i] = bestval
			ret_perr[i,i] = uncert**2
			if not quiet: print('special_curve_fit: p'+str(len(popt)-1-i)+' = ', bestval, ' +/- ', uncert, '( variation = ', variation, ', error = ', error, ')')
		return ret_popt, ret_perr

	######################################################## extract data ########################################################
	# fwhms
	fwhms1 = np.repeat(np.concatenate(tuple([total_data1[i]["fwhm"] for i in range(len(total_data1))])), 3)
	fwhms2 = np.repeat(np.concatenate(tuple([total_data2[i]["fwhm"] for i in range(len(total_data2))])), 3)
	fwhms3 = np.repeat(np.concatenate(tuple([total_data3[i]["fwhm"] for i in range(len(total_data3))])), 3)
	fwhms1_axes = np.concatenate(tuple([total_data1[i]["fwhm"] for i in range(len(total_data1))]))
	fwhms_all = np.concatenate((fwhms1, fwhms2, fwhms3))

	# corrected 3d dispersion
	disp_3d_corr1 = np.tile(np.concatenate(tuple([total_data1[i]["disp_corrs"][2] for i in range(len(total_data1))])), 3)
	disp_3d_corr2 = np.tile(np.concatenate(tuple([total_data2[i]["disp_corrs"][2] for i in range(len(total_data2))])), 3)
	disp_3d_corr3 = np.tile(np.concatenate(tuple([total_data3[i]["disp_corrs"][2] for i in range(len(total_data3))])), 3)
	disp_3d_corr1_axes = np.concatenate(tuple([total_data1[i]["disp_corrs"][2] for i in range(len(total_data1))]))
	
	# first moments
	first_moments1 = np.concatenate(tuple([np.concatenate(tuple(total_data1[i]["first_moments"])) for i in range(len(total_data1))]))
	first_moments2 = np.concatenate(tuple([np.concatenate(tuple(total_data2[i]["first_moments"])) for i in range(len(total_data2))]))
	first_moments3 = np.concatenate(tuple([np.concatenate(tuple(total_data3[i]["first_moments"])) for i in range(len(total_data3))]))

	# gradient corrected first moment correction factors
	corrected_first_moments1 = np.concatenate(tuple([np.concatenate(tuple(total_data1[i]["corrected_first_moments"])) for i in range(len(total_data1))]))
	corrected_first_moments2 = np.concatenate(tuple([np.concatenate(tuple(total_data2[i]["corrected_first_moments"])) for i in range(len(total_data2))]))
	corrected_first_moments3 = np.concatenate(tuple([np.concatenate(tuple(total_data3[i]["corrected_first_moments"])) for i in range(len(total_data3))]))
	corrected_first_moments1_x = np.concatenate(tuple([total_data1[i]["corrected_first_moments"][0] for i in range(len(total_data1))]))
	corrected_first_moments1_y = np.concatenate(tuple([total_data1[i]["corrected_first_moments"][1] for i in range(len(total_data1))]))
	corrected_first_moments1_z = np.concatenate(tuple([total_data1[i]["corrected_first_moments"][2] for i in range(len(total_data1))]))
	corrected_first_moments1_cf = disp_3d_corr1 / corrected_first_moments1
	corrected_first_moments2_cf = disp_3d_corr2 / corrected_first_moments2
	corrected_first_moments3_cf = disp_3d_corr3 / corrected_first_moments3
	corrected_first_moments1_x_cf = disp_3d_corr1_axes / corrected_first_moments1_x
	corrected_first_moments1_y_cf = disp_3d_corr1_axes / corrected_first_moments1_y
	corrected_first_moments1_z_cf = disp_3d_corr1_axes / corrected_first_moments1_z

	# second moment correction factors
	second_moments1 = np.sqrt(np.concatenate(tuple([np.concatenate(tuple(total_data1[i]["sigma_i2s"])) for i in range(len(total_data1))])))
	second_moments2 = np.sqrt(np.concatenate(tuple([np.concatenate(tuple(total_data2[i]["sigma_i2s"])) for i in range(len(total_data2))])))
	second_moments3 = np.sqrt(np.concatenate(tuple([np.concatenate(tuple(total_data3[i]["sigma_i2s"])) for i in range(len(total_data3))])))
	second_moments1_x = np.sqrt(np.concatenate(tuple([total_data1[i]["sigma_i2s"][0] for i in range(len(total_data1))])))
	second_moments1_y = np.sqrt(np.concatenate(tuple([total_data1[i]["sigma_i2s"][1] for i in range(len(total_data1))])))
	second_moments1_z = np.sqrt(np.concatenate(tuple([total_data1[i]["sigma_i2s"][2] for i in range(len(total_data1))])))
	second_moments1_cf = disp_3d_corr1 / second_moments1
	second_moments2_cf = disp_3d_corr2 / second_moments2
	second_moments3_cf = disp_3d_corr3 / second_moments3
	second_moments1_x_cf = disp_3d_corr1_axes / second_moments1_x
	second_moments1_y_cf = disp_3d_corr1_axes / second_moments1_y
	second_moments1_z_cf = disp_3d_corr1_axes / second_moments1_z

	# sigma_p correction factors
	sigma_ps1 = np.sqrt(first_moments1**2 + second_moments1**2)
	sigma_ps2 = np.sqrt(first_moments2**2 + second_moments2**2)
	sigma_ps3 = np.sqrt(first_moments3**2 + second_moments3**2)
	sigma_ps1_cf = disp_3d_corr1 / sigma_ps1
	sigma_ps2_cf = disp_3d_corr2 / sigma_ps2
	sigma_ps3_cf = disp_3d_corr3 / sigma_ps3

	# gradient corrected sigma_p correction factors
	corrected_sigma_ps1 = np.sqrt(corrected_first_moments1**2 + second_moments1**2)
	corrected_sigma_ps2 = np.sqrt(corrected_first_moments2**2 + second_moments2**2)
	corrected_sigma_ps3 = np.sqrt(corrected_first_moments3**2 + second_moments3**2)
	corrected_sigma_ps1_cf = disp_3d_corr1 / corrected_sigma_ps1
	corrected_sigma_ps2_cf = disp_3d_corr2 / corrected_sigma_ps2
	corrected_sigma_ps3_cf = disp_3d_corr3 / corrected_sigma_ps3
	corrected_sigma_ps_cf_all = np.concatenate((corrected_sigma_ps1_cf, corrected_sigma_ps2_cf, corrected_sigma_ps3_cf))

	########################################################## make fits ##########################################################

	# make corrected first moment fits
	first_fit1, first_cov1 = special_curve_fit(quadraticFunc, fwhms1, corrected_first_moments1_cf)
	first_fit2, first_cov2 = special_curve_fit(quadraticFunc, fwhms2, corrected_first_moments2_cf)
	first_fit3, first_cov3 = special_curve_fit(quadraticFunc, fwhms3, corrected_first_moments3_cf)
	first_fit1_x, first_cov1_x = special_curve_fit(quadraticFunc, fwhms1_axes, corrected_first_moments1_x_cf)
	first_fit1_y, first_cov1_y = special_curve_fit(quadraticFunc, fwhms1_axes, corrected_first_moments1_y_cf)
	first_fit1_z, first_cov1_z = special_curve_fit(quadraticFunc, fwhms1_axes, corrected_first_moments1_z_cf)

	# make second moment fits
	second_fit1, second_cov1 = special_curve_fit(linearFunc, fwhms1, second_moments1_cf)
	second_fit2, second_cov2 = special_curve_fit(linearFunc, fwhms2, second_moments2_cf)
	second_fit3, second_cov3 = special_curve_fit(linearFunc, fwhms3, second_moments3_cf)
	second_fit1_x, second_cov1_x = special_curve_fit(linearFunc, fwhms1_axes, second_moments1_x_cf)
	second_fit1_y, second_cov1_y = special_curve_fit(linearFunc, fwhms1_axes, second_moments1_y_cf)
	second_fit1_z, second_cov1_z = special_curve_fit(linearFunc, fwhms1_axes, second_moments1_z_cf)

	# make corrected sigma_p fits
	corr_sigma_p_fit1, corr_sigma_p_cov1 = special_curve_fit(linearFunc, fwhms1, corrected_sigma_ps1_cf)
	corr_sigma_p_fit_all, corr_sigma_p_cov_all = special_curve_fit(linearFunc, fwhms_all, corrected_sigma_ps_cf_all)

	############################################ output latex style file with fit data ############################################
	filename = "{}/best_fit_data_cotuff{}_r{}_latex.txt".format(save_info[0], save_info[2], save_info[3])
	f = open(filename, 'w+')
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^{y}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\siggradKDgen$ & $x$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][2],
		first_fit1_x[0], np.sqrt(first_cov1_x[0,0]),
		first_fit1_x[1], np.sqrt(first_cov1_x[1,1]),
		first_fit1_x[2], np.sqrt(first_cov1_x[2,2])))
	f.write("\n")
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^{y}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\siggradKDgen$ & $y$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][2],
		first_fit1_y[0], np.sqrt(first_cov1_y[0,0]),
		first_fit1_y[1], np.sqrt(first_cov1_y[1,1]),
		first_fit1_y[2], np.sqrt(first_cov1_y[2,2])))
	f.write("\n")
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^{z}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\siggradKDgen$ & $z$ & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][2],
		first_fit1_z[0], np.sqrt(first_cov1_z[0,0]),
		first_fit1_z[1], np.sqrt(first_cov1_z[1,1]),
		first_fit1_z[2], np.sqrt(first_cov1_z[2,2])))
	f.write("\n")
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 0.00$ & ")
	f.write(r'$\siggradKDgen$ & any & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][0],
		first_fit3[0], np.sqrt(first_cov3[0,0]),
		first_fit3[1], np.sqrt(first_cov3[1,1]),
		first_fit3[2], np.sqrt(first_cov3[2,2])))
	f.write("\n")
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 0.35$ & ")
	f.write(r'$\siggradKDgen$ & any & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][1],
		first_fit2[0], np.sqrt(first_cov2[0,0]),
		first_fit2[1], np.sqrt(first_cov2[1,1]),
		first_fit2[2], np.sqrt(first_cov2[2,2])))
	f.write("\n")
	f.write("$C_{\mathrm{(c} - \mathrm{grad)}}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\siggradKDgen$ & any & {0:.2f} & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$ & ${5:.2f} \pm {6:.2f}$\\'.format(
		save_info[4][2],
		first_fit1[0], np.sqrt(first_cov1[0,0]),
		first_fit1[1], np.sqrt(first_cov1[1,1]),
		first_fit1[2], np.sqrt(first_cov1[2,2])))
	f.write("\n")
	f.write("$C_\mathrm{i}^{x}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\sigtwoKDgen$ & $x$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][2],
		second_fit1_x[0], np.sqrt(second_cov1_x[0,0]),
		second_fit1_x[1], np.sqrt(second_cov1_x[1,1])))
	f.write("\n")
	f.write("$C_\mathrm{i}^{y}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\sigtwoKDgen$ & $y$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][2],
		second_fit1_y[0], np.sqrt(second_cov1_y[0,0]),
		second_fit1_y[1], np.sqrt(second_cov1_y[1,1])))
	f.write("\n")
	f.write("$C_\mathrm{i}^{z}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\sigtwoKDgen$ & $z$ & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][2],
		second_fit1_z[0], np.sqrt(second_cov1_z[0,0]),
		second_fit1_z[1], np.sqrt(second_cov1_z[1,1])))
	f.write("\n")
	f.write("$C_\mathrm{i}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 0.00$ & ")
	f.write(r'$\sigtwoKDgen$ & any & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][0],
		second_fit3[0], np.sqrt(second_cov3[0,0]),
		second_fit3[1], np.sqrt(second_cov3[1,1])))
	f.write("\n")
	f.write("$C_\mathrm{i}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 0.35$ & ")
	f.write(r'$\sigtwoKDgen$ & any & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][1],
		second_fit2[0], np.sqrt(second_cov2[0,0]),
		second_fit2[1], np.sqrt(second_cov2[1,1])))
	f.write("\n")
	f.write("$C_\mathrm{i}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\sigtwoKDgen$ & any & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][2],
		second_fit1[0], np.sqrt(second_cov1[0,0]),
		second_fit1[1], np.sqrt(second_cov1[1,1])))
	f.write("\n")
	f.write("$C_{\mathrm{(p} - \mathrm{grad)}}^\mathrm{any}$ for $E_\mathrm{rot}/E_\mathrm{turb} = 1.00$ & ")
	f.write(r'$\sigpgradKDgen$ & any & {0:.2f} & $0$ & ${1:.2f} \pm {2:.2f}$ & ${3:.2f} \pm {4:.2f}$\\'.format(
		save_info[4][2],
		corr_sigma_p_fit1[0], np.sqrt(corr_sigma_p_cov1[0,0]),
		corr_sigma_p_fit1[1], np.sqrt(corr_sigma_p_cov1[1,1])))
	f.write("\n")
	f.write("$C_{\mathrm{(p} - \mathrm{grad)}}^\mathrm{any}$ for any $E_\mathrm{rot}/E_\mathrm{turb}$ & ")
	f.write(r'$\sigpgradKDgen$ & any & any & $0$ & ${0:.2f} \pm {1:.2f}$ & ${2:.2f} \pm {3:.2f}$\\'.format(
		corr_sigma_p_fit_all[0], np.sqrt(corr_sigma_p_cov_all[0,0]),
		corr_sigma_p_fit_all[1], np.sqrt(corr_sigma_p_cov_all[1,1])))
	f.close()
	print("wrote fit data in latex table format to {}".format(filename))

	########################################################## make figure to see data ##########################################################
	fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(7, 8.4))
	x = np.linspace(0,2,500)

	ax1.plot(fwhms1, corrected_first_moments1_cf, "k.")
	ax1.plot(fwhms1, second_moments1_cf, "b.")
	ax2.plot(fwhms1, corrected_sigma_ps1_cf, "b.")
	ax2.plot(fwhms1, sigma_ps1_cf, "k.")

	ax1.plot(x, quadraticFunc(x, *first_fit1_x), "b-", label = r'$\sigma_{\mathrm{c-grad,}x}$')
	ax1.plot(x, quadraticFunc(x, *first_fit1_y), "g-", label = r'$\sigma_{\mathrm{c-grad,}y}$')
	ax1.plot(x, quadraticFunc(x, *first_fit1_z), "r-", label = r'$\sigma_{\mathrm{c-grad,}z}$')
	ax2.plot(x, linearFunc(x, *corr_sigma_p_fit1), "k-", label = r'$\sigma_{\mathrm{p-grad}}$')

	ax1.set_yscale('log')
	ax1.set_ylim(1,20)
	ax2.set_ylim(0.5,3.5)
	for ax in [ax1, ax2]:
		ax.set_xlim(0.008, 1)
		ax.set_xscale('log')
		ax.set_xticks([0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64])
		ax.xaxis.set_major_formatter(ScalarFormatter())
	ax1.legend()
	ax2.legend()

	filename = "{}/figure_fits_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3])
	plt.savefig(filename)
	print("saved {}".format(filename))


def figure7(total_data, save_info):
	# make labels
    second_moments = [r'$\sigma_{\mathrm{i}, x}$', r'$\sigma_{\mathrm{i}, y}$', r'$\sigma_{\mathrm{i}, z}$']
    x_axis_label = r'$\mathrm{Telescope \; beam \; size \; FWHM/R}$'
    y_axis_label = r'$\mathrm{Second\; moments \; (km/s)}$'
    heading = r'$\mathrm{Second \; moments \; as \; a \; function \; of \; beam \; resolution}$'

    # make data arrays
    fwhms = np.array([total_data[i,0] for i in range(len(total_data))])
    second_moments_x = np.array([total_data[i,1][0] for i in range(len(total_data))])
    second_moments_y = np.array([total_data[i,1][1] for i in range(len(total_data))])
    second_moments_z = np.array([total_data[i,1][2] for i in range(len(total_data))])

    fwhms_x_y = np.concatenate((fwhms, fwhms))
    second_moments_x_y = np.concatenate((second_moments_x, second_moments_y))
    second_fit, second_cov = curve_fit(linearFunc_int0, fwhms_x_y, second_moments_x_y)
    print("second moment = ({}+-{}) * f/pc".format(second_fit[0], np.sqrt(second_cov[0,0])))
    grad_err = np.sqrt(second_cov[0,0])
    x = np.linspace(0,0.7,500)

    fig, ax = plt.subplots()

    ax.plot(fwhms, second_moments_x, "bx", label = second_moments[0])
    ax.plot(fwhms, second_moments_y, "g+", label = second_moments[1], markersize = 7.5)
    ax.plot(fwhms, second_moments_z, "rs", label = second_moments[2])
    ax.plot(x, linearFunc_int0(x, second_fit[0]),"g-")
    ax.plot(x, linearFunc_int0(x, second_fit[0]-grad_err),"g--")
    ax.plot(x, linearFunc_int0(x, second_fit[0]+grad_err),"g--")
    ax.plot(x, linearFunc_int0(x, 0),"r-")

    # make headings and labels
    fontsize = 25
    bbox = dict(facecolor='white', edgecolor='white', boxstyle='round', pad = 0.01)
    ax.set_xlabel(x_axis_label, fontsize = fontsize)
    ax.set_ylabel(y_axis_label, fontsize = fontsize)
    ax.text(0.02, 0.98, heading, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes, fontsize = fontsize, bbox = bbox)

    # make legend
    ax.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), fontsize = fontsize)

    # set limits
    ax.set_xlim(0.008, 1)
    #ax.set_ylim(0, 0.4)
    ax.set_xscale('log')
    #ax.set_yscale('log')
    ax.set_xticks([0.01, 0.02, 0.04, 0.08, 0.16, 0.32, 0.64])
    ax.xaxis.set_major_formatter(ScalarFormatter())
    ax.tick_params(axis='x', which='minor', bottom=False, top=False)
    ax.tick_params(axis = 'both', which = 'major', labelsize = fontsize)
    ax.tick_params(axis = 'both', which = 'minor', labelsize = fontsize)
	#ax.xaxis.set_major_locator(MultipleLocator(0.5))

    plt.savefig("{}/figure7_{}_{}.pdf".format(save_info[0], save_info[1], save_info[2]))
    print("saved {}/figure7_{}_{}.pdf".format(save_info[0], save_info[1], save_info[2]))

def figure_Dickman_Kleiner(t, sigma_c2, sigma_i2, sigma_3d, save_info, total = False):
    if total:
        # calculate relevant statistics
        sigma_c2 = np.sqrt(sigma_c2[0]**2 + sigma_c2[1]**2 + sigma_c2[2]**2)
        sigma_i2 = np.sqrt(sigma_i2[0]**2 + sigma_i2[1]**2 + sigma_i2[2]**2)
        sigma_p2 = sigma_c2 + sigma_i2
    
        fig, ax = plt.subplots()
    
        # set labels
        sigma_c2_label = r'$\sigma_\mathrm{c}^2$'
        sigma_i2_label = r'$\sigma_\mathrm{i}^2$'
        sigma_p2_label =r'$\sigma_\mathrm{p}^2$'
        sigma_3d_label = r'$\sigma _{v_\mathrm{3D}}^2$'
        heading = r'$\mathrm{Velocity\; dispersions}$'
        y_axis_label = r'$\mathrm{Velocity\; dispersion\; (km/s)^2}$'
        x_axis_label = r'$\mathrm{Time\; (percentage\; of\; freefall\; time)}$'
    
        # plot data
        ax.plot(t, sigma_c2, "x-", label = sigma_c2_label)
        ax.plot(t, sigma_i2, "s-", label = sigma_i2_label)
        ax.plot(t, sigma_p2, "*-", label = sigma_p2_label)
        ax.plot(t, sigma_3d, ".-", label = sigma_3d_label)
    
        # set axes labels and limits as well as heading
        ax.set_xlabel(x_axis_label)
        ax.set_ylabel(y_axis_label)
        ax.set_xlim(0, 100)
        ax.set_ylim(0, 2)
        ax.text(0.02, 0.98, heading, horizontalalignment = 'left', verticalalignment = 'top', transform = ax.transAxes)
        # make legend
        column_space = 1
        ax.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), ncol = 3, columnspacing = column_space)
    
    else:
        # calculate sigma_p^2
        sigma_p2 = sigma_c2 + sigma_i2 # *** check that sigmac^2 and sigma i^2 are np.arrays.
    
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (16, 4.8)) # original (20,6)
    
        # Set labels
        sigma_c2_labels = [r'$\sigma_{\mathrm{c}, x}^2$', r'$\sigma_{\mathrm{c}, y}^2$', r'$\sigma_{\mathrm{c}, z}^2$']
        sigma_i2_labels = [r'$\sigma_{\mathrm{i}, x}^2$', r'$\sigma_{\mathrm{i}, y}^2$', r'$\sigma_{\mathrm{i}, z}^2$']
        sigma_p2_labels = [r'$\sigma_{\mathrm{p}, x}^2$', r'$\sigma_{\mathrm{p}, y}^2$', r'$\sigma_{\mathrm{p}, z}^2$']
        sigma_p2_sum_label = r'$\sigma_{\mathrm{p}, \mathrm{3D}}^2$' #r'$\sigma_{p; \> x}^2 + \sigma_{p; \> y}^2 + \sigma_{p; \> z}^2$'
        sigma_3d_label = r'$\sigma _{v_\mathrm{3D}}^2$'
        heading1 = r'$\mathrm{Velocity\; dispersions\; based\; on\; 1st\; moment}$'
        heading2 = r'$\mathrm{Velocity\; dispersions\; based\; on\; 1st\; and\; 2nd\; moment}$'
        y_axis_label = r'$\mathrm{Velocity\; dispersion\; (km/s)^2}$'
        x_axis_label = r'$\mathrm{Time\; (percentage\; of\; freefall\; time)}$'
    
        markersize = 3.2 # was 4
        # First moment panel
        ax1.plot(t, sigma_c2[0], "bx-", label = sigma_c2_labels[0], markersize = markersize)
        ax1.plot(t, sigma_i2[0], "bs-", label = sigma_i2_labels[0], markersize = markersize)
        ax1.plot(t, sigma_c2[1], "gx-", label = sigma_c2_labels[1], markersize = markersize)
        ax1.plot(t, sigma_i2[1], "gs-", label = sigma_i2_labels[1], markersize = markersize)
        ax1.plot(t, sigma_c2[2], "rx-", label = sigma_c2_labels[2], markersize = markersize)
        ax1.plot(t, sigma_i2[2], "rs-", label = sigma_i2_labels[2], markersize = markersize)
    
        # Second moment panel
        ax2.plot(t, sigma_p2[0], "b*-", label = sigma_p2_labels[0], markersize = markersize)
        ax2.plot(t, sigma_p2[0] + sigma_p2[1] + sigma_p2[2], "m.-", label = sigma_p2_sum_label)
        ax2.plot(t, sigma_p2[1], "g*-", label = sigma_p2_labels[1], markersize = markersize)
        ax2.plot(t, sigma_3d**2, "y.-", label = sigma_3d_label)
        ax2.plot(t, sigma_p2[2], "r*-", label = sigma_p2_labels[2], markersize = markersize)
    
        # Make labels and set limits
        for ax in [ax1, ax2]:
            ax.set_xlabel(x_axis_label)
            ax.set_ylabel(y_axis_label)
            ax.set_xlim(0, 100)
        ax2.set_ylim(0, 3.8)
        ax1.set_ylim(0, 1.3)

        ax1.text(0.02, 0.98, heading1, horizontalalignment = 'left', verticalalignment = 'top', transform = ax1.transAxes)
        ax2.text(0.02, 0.98, heading2, horizontalalignment = 'left', verticalalignment = 'top', transform = ax2.transAxes)

        column_space = 1
        ax1.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), ncol = 3, columnspacing = column_space)
        ax2.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), ncol = 3, columnspacing = column_space)
    
    plt.savefig("{}/figure_comparison_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3])) #images path, sim id, cutoff, decrease res
    plt.clf()
    plt.close()
    print("saved {}/figure_comparison_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3]))

def figure_Dickman_Kleiner_corr(t, sigma_c_corr2, sigma_i2, sigma_3d, save_info, correction_factors = False):
# calculate sigma_p,corr^2
    sigma_p_corr2 = sigma_c_corr2 + sigma_i2 # *** check that sigmac^2 and sigma i^2 are np.arrays.
    sigma_p_corr_correction_factors = sigma_3d / np.sqrt(sigma_p_corr2)
    
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (16, 4.8)) # original (20,6)
    
    # Set labels
    sigma_c_corr2_labels = [r'$\sigma_{\mathrm{(c}-\mathrm{grad)}, x}^2$',r'$\sigma_{\mathrm{(c}-\mathrm{grad)}, y}^2$', r'$\sigma_{\mathrm{(c}-\mathrm{grad)},z}^2$']
    sigma_i2_labels = [r'$\sigma_{\mathrm{i}, x}^2$', r'$\sigma_{\mathrm{i}, y}^2$', r'$\sigma_{\mathrm{i}, z}^2$']
    sigma_p_corr2_sum_label = r'$\sigma_{\mathrm{(p}-\mathrm{grad),3D}}^2$' #r'$\sigma_{p; \> x}^2 + \sigma_{p; \> y}^2 + \sigma_{p; \> z}^2$'
    sigma_3d_label = r'$\sigma _{v_\mathrm{3D}}^2$'
    x_axis_label = r'$\mathrm{Time\; (percentage\; of\; freefall\; time)}$'
    heading1 = r'$\mathrm{Velocity\; dispersions\; based\; on\; corrected\; 1st\; moment}$'
    if correction_factors:
    	y_axis_label = r'$\mathrm{Correction factors}$'
    	heading2 = r'$\mathrm{Correction \; factors\; based\; on\; corrected\; 1st\; and\; 2nd\; moment}$'
    	sigma_p_corr2_labels = [r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, x}$', r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, y}$', r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, z}$']
    else:
    	y_axis_label = r'$\mathrm{Velocity\; dispersion\; (km/s)^2}$'
    	heading2 = r'$\mathrm{Velocity\; dispersions\; based\; on\; corrected\; 1st\; and\; 2nd\; moment}$'
    	sigma_p_corr2_labels = [r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, x}^2$', r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, y}^2$', r'$\sigma_{\mathrm{(p}-\mathrm{grad)}, z}^2$']
    
    markersize = 3.2 # was 4
    # First moment panel
    ax1.plot(t, sigma_c_corr2[0], "bx-", label = sigma_c_corr2_labels[0], markersize = markersize)
    ax1.plot(t, sigma_i2[0], "bs-", label = sigma_i2_labels[0], markersize = markersize)
    ax1.plot(t, sigma_c_corr2[1], "gx-", label = sigma_c_corr2_labels[1], markersize = markersize)
    ax1.plot(t, sigma_i2[1], "gs-", label = sigma_i2_labels[1], markersize = markersize)
    ax1.plot(t, sigma_c_corr2[2], "rx-", label = sigma_c_corr2_labels[2], markersize = markersize)
    ax1.plot(t, sigma_i2[2], "rs-", label = sigma_i2_labels[2], markersize = markersize)
    
    # Second moment panel
    if correction_factors:
    	ax2.plot(t, sigma_p_corr_correction_factors[0], "b*-", label = sigma_p_corr2_labels[0], markersize = markersize)
    	ax2.plot(t, sigma_p_corr_correction_factors[1], "g*-", label = sigma_p_corr2_labels[1], markersize = markersize)
    	ax2.plot(t, sigma_3d**2, "y.-", label = sigma_3d_label)
    	ax2.plot(t, sigma_p_corr_correction_factors[2], "r*-", label = sigma_p_corr2_labels[2], markersize = markersize)
    else:
	    ax2.plot(t, sigma_p_corr2[0], "b*-", label = sigma_p_corr2_labels[0], markersize = markersize)
    	#ax2.plot(t, sigma_p_corr2[0] + sigma_p_corr2[1] + sigma_p_corr2[2], "m.-", label = sigma_p_corr2_sum_label)
	    ax2.plot(t, sigma_p_corr2[1], "g*-", label = sigma_p_corr2_labels[1], markersize = markersize)
	    ax2.plot(t, sigma_3d**2, "y.-", label = sigma_3d_label)
	    ax2.plot(t, sigma_p_corr2[2], "r*-", label = sigma_p_corr2_labels[2], markersize = markersize)
    
    # Make labels and set limits
    for ax in [ax1, ax2]:
        ax.set_xlabel(x_axis_label)
        ax.set_ylabel(y_axis_label)
        ax.set_xlim(0, 100)
    ax2.set_ylim(0, 3.8)
    ax1.set_ylim(0, 1.3)

    ax1.text(0.02, 0.98, heading1, horizontalalignment = 'left', verticalalignment = 'top', transform = ax1.transAxes)
    ax2.text(0.02, 0.98, heading2, horizontalalignment = 'left', verticalalignment = 'top', transform = ax2.transAxes)

    column_space = 1
    ax1.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), ncol = 3, columnspacing = column_space)
    ax2.legend(loc = 'upper left', bbox_to_anchor=(0, 0.95), ncol = 3, columnspacing = column_space)
    
    plt.savefig("{}/figure_comparison_corrected_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3])) #images path, sim id, cutoff, decrease res
    plt.clf()
    plt.close()
    print("saved {}/figure_comparison_corrected_{}_cutoff{}_res{}.pdf".format(save_info[0], save_info[1], save_info[2], save_info[3]))

