# A new method for measuring the 3D turbulent velocity dispersion from position-position-velocity observations of interstellar clouds
This repository contains the scripts used to analyse FLASH simulations of interstellar clouds published in Stewart & Federrath (2022):
https://ui.adsabs.harvard.edu/abs/2022MNRAS.509.5237S/abstract

If you are using this code, please refer to and cite Stewart & Federrath (2022, MNRAS 509, 5237).

*"-h"* can be used to find out more about each keyword taken by each script.

---

## File system
These scripts work assuming the following relative file system exists.

- ***parent directory***
    - ***scripts*** (to be located in *parent direcotry*)
    The scripts contained in this repository should be located in this folder.
    - ***flash_data*** (to be located in *parent direcotry*)
    This folder should be named *"flash_data"* and should contain the FLASH data for all simulations organised in sub-folders.
        - ***sim_id*** (to be located in *flash_data*)
        For each simulation, there should be a folder containing the *.plt* files to be analysed.
    - ***output*** (to be located in *parent direcotry*)
    This folder will be created automatically and will contain the output data and images from the scripts organised according to simulation, density cutoff threshold, the dump number and the fwhm.

---

## Scripts

### main_script.py and main_script_functions.py
*main_script.py* analyses each *.plt* file calculating the moment map data and 3D dispersion data. Many of the functions which *main_script.py* calls to are defined in *main_script_functions.py*.

#### Statistics calculated
For a given dump number or for each dump in a given range, *main_script.py* calculates the zero-moment map, first-moment map and second-moment map and also fits a gradient to the first-moment map and subtracts this from the first-moment map to produce the gradient-corrected first-moment map. This is done for synthetic observations of the cloud along the x,y and z axes. Images of these maps can be generated using the *--images* keyword.

The standard deviations of the first-moment maps, standard deviations of the gradient-corrected first-moment maps and the spatial means of the second-moment maps are calculated and output to a "Turbulence_Data" file for each given dump.

With a given angular frequency, the rotation velocity of the gas in each cell is calculated assuming rigid body rotation of the cloud about the z-axis and these velocities are subtracted to attain the "turbulent velocities". The standard deviations of the 3D velocities of each cell, the 3D turbulent velocities of each cell, the x, y and z-components of the velocities of each cell and the x and y components of the turbulent velocities of each cell are calculated and written to the corresponding "Turbulence_Data" file for each dump.

---

### total_turbulence_data.py
*total_turbulence_data.py* collates the data in each "Turbulence_Data" file of dumps in a given range producing a "total_turbulence_data" text file which can be read to analyse the cloud over time and to produce images.

---

### paper_images.py and paper_images_functions.py
*paper_images.py* is used to make the images in the paper. Many of the functions which *paper_images.py* calls to are defined in *paper_images_functions.py*. For each image, the simulation, line of sight axis, fwhms, density cutoff threshold and dumps used can be changed with key word arguments.

#### Possible images
##### --fig1
A three by three grid of moment maps as viewed along a given line of sight (default is y). Each column is a different time in the simulation. The first row is the zero-moment map, the second row is the first-moment map and the third row is the second-moment map.

##### --fig2
This image shows the gradient correction of the first moment process. The top row shows the first-moment map, the gradient fit to the first-moment map and the gradient-corrected first-moment map. The bottom row show the velocity pdf of the first-moment map and the gradient-corrected first-moment map.

##### --fig3
This image shows the time evolution of statistics calculated by *main_script.py*.

##### --fig4
This image shows the time evolution of the "correction factor" necessary to recover the 3D turbulent velocity dispersion from both the standard deviation of the gradient-corrected first-moment map and the spatial mean of the second-moment map.

##### --fig5
This image shows the dependence of the time and line of sight averaged correction factor on the telescope beam resolution. To make this image, the "time_averages_data_table" must have been made for each fwhm included in the image, this is made using *--table*. The lines of best fit are written to a "resolution_study_fit_data" text file. This image has a log scale, but *--linear* keyword can be used to make an image with a linear scale.

##### --fig6
This image is the same as --fig5 but the time averaged correction factors are given for each line of sight at each telescope beam resolution. To make this image, the "time_averages_data_table" must have been made for each fwhm included in the image, this is made using *--table*. The lines of best fit are written to a "resolution_study_fit_data_axes" text file. This image has a log scale, but *--linear* keyword can be used to make an image with a linear scale.

##### --table
This produces a text file containing all the time averages of various statistics. This table is used to make *--fig5* and *--fig6*.

___

### matplotlibrc.py
This file configures some matplotlib settings for the images made in *paper_images.py*.
