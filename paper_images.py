#!/usr/bin/env python

# import all relevant modules
import numpy as np
print("imported numpy")
import argparse
print("imported argparse")
import os
print("imported os")
import pdb
print("imported pdb")
import main_script_functions as msf
print("imported main_script_functions")
import paper_images_functions as pif

parser = argparse.ArgumentParser()
parser.add_argument('--sphere_test', default = 1, help = "Identity of sphere test, default is 1", type = int)
parser.add_argument('--sphere_test1', default = 44, help = "Identity of second sphere test, default is 44", type = int)
parser.add_argument('--sphere_test2', default = 87, help = "Identity of third sphere test, default is 87", type = int) #89 for with_turb
parser.add_argument('--axis', default = 'y', help = "Line of sight to make images from, default is y", type = str)
parser.add_argument('--fwhm', default = 1.1636, help = "Telescope fwhm in pixels, default is 1.1636 (1 pc)", type = float)
parser.add_argument('--fwhms', default = "1.1636,2.3272,4.6544,9.3088,18.6176,37.2352,74.4704", help = "Decrease resolution factors for figure_res", type = str) #,148.9408
parser.add_argument('--sim_id', default = "high_rot", help = "simulation identification, default is with_turb", type = str)
parser.add_argument('--sim_id2', default = "with_turb", help = "simulation identification, default is with_turb", type = str)
parser.add_argument('--sim_id3', default = "turb_only", help = "simulation identification, default is with_turb", type = str)
parser.add_argument('--cutoff', default = 0.1, help = "Cut off every cell with a value less than this in the zero moment contrast map, defualt is 0.1", type = float)
parser.add_argument('--fig1', help = "make figure 1", action="store_true")
parser.add_argument('--fig2', help = "make figure 2", action="store_true")
parser.add_argument('--fig3', help = "make figure 3", action="store_true")
parser.add_argument('--fig4', help = "make figure 4", action="store_true")
parser.add_argument('--fig5', help = "make figure 5, run --fits first", action="store_true")
parser.add_argument('--fig6', help = "make figure 6,  run --fits first", action="store_true")
parser.add_argument('--fig7', help = "make figure 7 (second moment dependence on resolution)", action="store_true")
parser.add_argument('--fig_comp', help = "make figure like Dieckman and Klenier", action="store_true")
parser.add_argument('--corr_fac', help = "make figure like Dieckman and Klenier with correction factor version for corrected sigma_p", action="store_true")
parser.add_argument('--table', help = "make data table", action="store_true")
parser.add_argument('--fits', help = "Determine best fits for figures 5 and 6 and make a latex style table. Run this before running either figure 5 or 6.", action="store_true")
parser.add_argument('--linear', help = "make figure 2, 5 and/or 6 with a linear scale", action="store_true")
args = parser.parse_args()

sphere_test = str(args.sphere_test).zfill(4)
sphere_test1 = str(args.sphere_test1).zfill(4)
sphere_test2 = str(args.sphere_test2).zfill(4)
simulation_id = args.sim_id

# Make the cutoff string
if np.isnan(args.cutoff):
	cutoff_str = "no_cutoff"
else:
	cutoff_str = str(args.cutoff)

# Load maps data
images_path = "../output/{}/images/{}/paper_images".format(simulation_id, args.cutoff)
images_path2 = "../output/{}/images/{}/paper_images".format(args.sim_id2, args.cutoff)
images_path3 = "../output/{}/images/{}/paper_images".format(args.sim_id3, args.cutoff)
output_path =  msf.get_output_path(simulation_id, cutoff_str, sphere_test, args.fwhm)
output_path1 =  msf.get_output_path(simulation_id, cutoff_str, sphere_test1, args.fwhm)
output_path2 =  msf.get_output_path(simulation_id, cutoff_str, sphere_test2, args.fwhm)

# Make the output path if it does not exist
if os.path.exists(images_path) == False:
	os.makedirs(images_path)
	print("made directory: {}".format(output_path))

######################################################## Data for Figures 1 and 2 ########################################################
if args.fig1 or args.fig2:
	# Get data file path
	flash_data_path = msf.get_flash_data_path(simulation_id, sphere_test)

	# Load the data
	properties, x, y, z = msf.load_data(flash_data_path, output_path, sphere_test, args.fwhm, need_all = True)
	properties1, x1, y1, z1 = msf.load_data(flash_data_path, output_path1, sphere_test1, args.fwhm, need_all = True)
	properties2, x2, y2, z2 = msf.load_data(flash_data_path, output_path2, sphere_test2, args.fwhm, need_all = True)
	
	# Define the times and minimas and maximas of all dumps
	times = [properties["*time"], properties1["*time"], properties2["*time"]] #time should be a 1D array, so there should be a [0] after each. Not sure what's going on
	times_rel = [time / 1.652319644248078e13 for time in times] # calculated freefall time = 1.652319644248078e13 # was 163782548966.54752
	min_maxs = [properties["*min_max"], properties1["*min_max"], properties2["*min_max"]]
	
	# Determine index of zero moment based on axis
	if args.axis == 'x':
		los = x
		los1 = x1
		los2 = x2
		min_maxs = [np.delete(min_max, [0,1]) for min_max in min_maxs]	
	elif args.axis == 'y':
		los = y
		los1 = y1
		los2 = y2
		min_maxs = [np.delete(min_max, [2,3]) for min_max in min_maxs]
	elif args.axis == 'z':
		los = z
		los1 = z1
		los2 = z2
		min_maxs = [np.delete(min_max, [4,5]) for min_max in min_maxs]
	else:
		raise RuntimeError ("Line of sight axis was not specified as x, y, or z")
	
	zero_dict = {"0": los["zero_moment"], "1": los1["zero_moment"], "2": los2["zero_moment"]}
	first_dict = {"0": los["first_moment"], "1": los1["first_moment"], "2": los2["first_moment"]}
	plane_fits_dict = {"0": los["planefit"], "1": los1["planefit"], "2": los2["planefit"]}
	second_dict = {"0": los["second_moment"], "1": los1["second_moment"], "2": los2["second_moment"]}
	firstz_dict = {"0": z["first_moment"], "1": z["first_moment"], "2": z["first_moment"]}
	plane_fitsz_dict = {"0": z["planefit"], "1": z["planefit"], "2": z["planefit"]}
		
	for key in sorted(zero_dict):
		# define data
		# decrease the resolution of observed maps by gaussian smoothing
		fwhm_new = args.fwhm
		zero_dict[key] = msf.gauss_smooth(zero_dict[key], fwhm = fwhm_new)
		first_dict[key] = msf.gauss_smooth(first_dict[key], fwhm = fwhm_new)
		second_dict[key] = msf.gauss_smooth(second_dict[key], fwhm = fwhm_new)
		firstz_dict[key] = msf.gauss_smooth(firstz_dict[key], fwhm = fwhm_new)
		
		contrast = np.divide(zero_dict[key], np.nanmean(zero_dict[key]))
	
		# apply cutoff
		if ~np.isnan(args.cutoff):
			mask = contrast < args.cutoff

			zero_dict[key][mask] = np.nan
			first_dict[key][mask] = np.nan
			plane_fits_dict[key][mask] = np.nan
			second_dict[key][mask] = np.nan
			firstz_dict[key][mask] = np.nan
			plane_fitsz_dict[key][mask] = np.nan

	# make array of first moment data for image 2 from sphere test 2
	first_moment_data = [first_dict["1"], plane_fits_dict["1"], first_dict["1"] - plane_fits_dict["1"]]
	first_moment_dataz = [firstz_dict["1"], plane_fitsz_dict["1"], firstz_dict["1"] - plane_fitsz_dict["1"]]

####################################################### Turbulence quantifier data #######################################################
def turbulence_quantifier_data(sim_id, fwhm, cutoff):
	# Load all time data
    # Make file name
    total_data_location = "../output/{}/data/total_turbulence_data_r{}_{}.txt".format(sim_id, fwhm, cutoff)
	
    # Get data file as csv
    if os.path.exists(total_data_location):
        print("extracting data from {}".format(total_data_location))
        data = np.genfromtxt(total_data_location, delimiter = ',')
    else:
        print("error: total turbulence data file does not exist; {}\n".format(total_data_location))
        exit()
	
    # 3d dispersion
    disp_x = data[1:, 15]
    disp_y = data[1:, 16]
    disp_z = data[1:, 17]
    disp_x_corr = data[1:, 18]
    disp_y_corr = data[1:, 19]
    disp_3d = data[1:, 13]
    disp_3d_corr = data[1:, 14]
	
    # x axis
    x_first_moment = data[1:, 1]
    x_first_moment_corrected = data[1:,2]
    x_second_moment = data[1:, 3]
    x_sigma_i2 = data[1:, 4]
	
    # y axis
    y_first_moment = data[1:, 5]
    y_first_moment_corrected = data[1:,6]
    y_second_moment = data[1:, 7]
    y_sigma_i2 = data[1:, 8]
	
	# z axis
    z_first_moment = data[1:, 9]
    z_first_moment_corrected = data[1:,10]
    z_second_moment = data[1:, 11]
    z_sigma_i2 = data[1:, 12]
	
	
	# Time data
    t = data[1:,0]
    t /= 16378254896654.752
    t *= 100

    # fwhm
    fwhm_pc = fwhm * 2.2 / 256
    fwhms = np.tile(np.array([fwhm_pc]),len(t))
	
    first_moments = np.array([x_first_moment, y_first_moment, z_first_moment])
    first_moments_corrected = np.array([x_first_moment_corrected, y_first_moment_corrected, z_first_moment_corrected])
    second_moments = [x_second_moment, y_second_moment, z_second_moment]
    sigma_i2s = np.array([x_sigma_i2, y_sigma_i2, z_sigma_i2])
    disps = [disp_x, disp_y, disp_z, disp_3d]
    disp_corrs = [disp_x_corr, disp_y_corr, disp_3d_corr]

    total_data = {"first_moments": first_moments, "corrected_first_moments": first_moments_corrected,
    				"second_moments": second_moments, "sigma_i2s": sigma_i2s,
    				"disps": disps, "disp_corrs": disp_corrs, "t": t, "fwhm": fwhms}

    return total_data

######################################################## Data for Figures 4 and 5 ########################################################
if args.fig3 or args.fig4 or args.fig_comp or args.table:
	all_data = turbulence_quantifier_data(args.sim_id, args.fwhm, cutoff_str)

######################################################## Make figures ########################################################
# get rotational to turbulent energy ratio
if simulation_id != "high_rot" or args.sim_id2 != "with_turb" or args.sim_id3 != "turb_only":
	print("Simulation ids are not the default. Please exit debug mode and enter the rotation to turbulent energy ratios for all three simulations to 2 decimal places separated by commas (or enter 'exit' to stop program).")
	pdb.set_trace()
	user_input = input("E_rot / E_turb:")
	if user_input == "exit":
		exit()
	e_ratios = [float(e_ratio) for e_ratio in user_input.split(",")]
else:
	e_ratios = [0.00, 0.35, 1.00]

save_info = [images_path, simulation_id, cutoff_str, args.fwhm, e_ratios]
if args.fig1:
	pif.figure1(args.axis, times_rel, zero_dict, first_dict, second_dict, min_maxs, save_info)
	
if args.fig2:
	pif.figure2(args.axis, first_moment_data, min_maxs[0], save_info, linear = args.linear)

if args.fig3:
	pif.figure3(all_data["t"], all_data["first_moments"], all_data["corrected_first_moments"],
		np.sqrt(all_data["sigma_i2s"]), all_data["disp_corrs"][2], save_info)

if args.fig4:
	pif.figure4(all_data["t"], all_data["first_moments"], all_data["corrected_first_moments"],
		np.sqrt(all_data["sigma_i2s"]), all_data["disp_corrs"][2], save_info)

if args.fig5:
	fwhms = list(map(float, args.fwhms.split(",")))
	data = [pif.get_fig_res_data(images_path, simulation_id, cutoff_str, fwhm) for fwhm in fwhms]
	pif.figure5(data, save_info, args.linear)

if args.fig6:
	fwhms = list(map(float, args.fwhms.split(",")))
	data1 = [pif.get_fig_res_data(images_path, simulation_id, cutoff_str, fwhm) for fwhm in fwhms]
	data2 = [pif.get_fig_res_data(images_path2, args.sim_id2, cutoff_str, fwhm) for fwhm in fwhms]
	data3 = [pif.get_fig_res_data(images_path3, args.sim_id3, cutoff_str, fwhm) for fwhm in fwhms]
	# entered in order from lowest to highest rotation
	pif.figure6(data3, data2, data1, save_info)

if args.fig7:
	data_path = "../output/{}/data/0000".format(simulation_id)
	fwhms = list(map(float, args.fwhms.split(",")))
	data = np.array([pif.get_second_moment_data(data_path, cutoff_str, fwhm) for fwhm in fwhms])
	pif.figure7(data, save_info)

if args.fig_comp:
    pif.figure_Dickman_Kleiner(t, first_moments**2, sigma_i2s, disp_3d, save_info)
    if args.corr_fac:
    	pif.figure_Dickman_Kleiner_corr(t, first_moments_corrected**2, sigma_i2s, disp_3d, save_info, correction_factors = True)
    else:
    	pif.figure_Dickman_Kleiner_corr(t, first_moments_corrected**2, sigma_i2s, disp_3d, save_info)

if args.table:
	pif.table_data(all_data["t"], all_data["first_moments"], all_data["corrected_first_moments"],
		np.sqrt(all_data["sigma_i2s"]), all_data["disp_corrs"][2], save_info)

if args.fits:
	fwhms = list(map(float, args.fwhms.split(",")))
	total_data1 = [turbulence_quantifier_data(simulation_id, fwhm, cutoff_str) for fwhm in fwhms]
	total_data2 = [turbulence_quantifier_data(args.sim_id2, fwhm, cutoff_str) for fwhm in fwhms]
	total_data3 = [turbulence_quantifier_data(args.sim_id3, fwhm, cutoff_str) for fwhm in fwhms]
	pif.best_fits(total_data1, total_data2, total_data3, save_info)
